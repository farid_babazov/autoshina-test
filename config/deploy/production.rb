set :deploy_to, "/home/deploy/apps/#{application}"
role :web, "deploy@172.104.245.178"
role :app, "deploy@172.104.245.178"
role :db,  "deploy@172.104.245.178", :primary => true # This is where Rails migrations will run
