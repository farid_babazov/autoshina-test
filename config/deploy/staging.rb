set :deploy_to, "/data/#{application}/app"
set :stage, :staging
set :repository,  "git@rscz.ru:rocket-station/autoshina.git"
set :rvm_ruby_string, "2.5.0@#{application}"

# Bug with current_path in capistrano. Donnow why, but this should fix
set :current_path, File.join(deploy_to, current_dir)

role :web, "autoshina@rs0.ru"
role :app, "autoshina@rs0.ru"
role :db,  "autoshina@rs0.ru", :primary => true # This is where Rails migrations will run

set :rails_env, 'staging'

namespace :deploy do
  desc "Symlink database"
  task :symlink_database do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end

before "deploy:assets:precompile", "deploy:symlink_database"
