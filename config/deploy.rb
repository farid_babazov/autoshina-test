require "bundler/capistrano"
require "rvm/capistrano"
require "puma/capistrano"
require "whenever/capistrano"
require 'capistrano/ext/multistage'
# require 'capistrano/sidekiq'
set :stages, %w(staging production)
set :user, "deploy"
set :application, "autoshina"

set :scm, :git
# set :repository,  "git@bitbucket.org:autoshinakg/autoshina.git"
set :repository,  "git@rscz.ru:rocket-station/autoshina.git"
set :deploy_via, :remote_cache

set :branch, ENV["REVISION"] || "master"

set :use_sudo, false

set :rvm_ruby_string, "ruby-2.0.0-p353@#{application}.kg"
# set :rvm_ruby_string, "ruby-2.0.0-p648@#{application}.kg"
# role :web, "deploy@172.104.245.178"
# role :app, "deploy@172.104.245.178"
# role :db,  "deploy@172.104.245.178", :primary => true # This is where Rails migrations will run
# role :web, "deploy@10.0.0.20"
# role :app, "deploy@10.0.0.20"
# role :db,  "deploy@10.0.0.20", :primay => true

set :whenever_command, "bundle exec whenever"
set :sidekiq_cmd, "bundle exec sidekiq"
set :sidekiqctl_cmd, "bundle exec sidekiqctl"
set :sidekiq_timeout, 10
set :sidekiq_role, :app
set :sidekiq_pid, "#{current_path}/tmp/pids/sidekiq.pid"
set :sidekiq_processes, 1

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

before  "deploy:setup", "rvm:install_ruby"
before  "deploy:setup", "rvm:create_gemset"
after   "deploy:assets:precompile", "customs:set_env"
after   "deploy", "deploy:cleanup"

namespace :customs do
  desc 'Setting a symlink to .env'
  task :set_env, roles: :app do
    run "ln -nfs #{shared_path}/config/.env #{release_path}/.env"
  end
end

namespace :deploy do
  desc "Invoke rake task"
  task :invoke do
    run "cd '#{current_path}' && #{rake} #{ENV['task']} RAILS_ENV=#{rails_env}"
  end
end

namespace :rails do
  desc "Remote console"
  task :console, :roles => :app do
    run_interactively "bundle exec rails console #{rails_env}"
  end

  desc "Remote dbconsole"
  task :dbconsole, :roles => :app do
    run_interactively "bundle exec rails dbconsole #{rails_env}"
  end
end

def run_interactively(command, server=nil)
  server ||= find_servers_for_task(current_task).first
  exec %Q(ssh -l #{user} #{server.host} -t 'bash --login -c "cd #{current_path} && #{command}"')
end
