# require 'sidekiq/web'
AutoshinaKg::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  mount Ckeditor::Engine => '/ckeditor'
  # mount Sidekiq::Web => '/sidekiq'
  # authenticate :admin_user do
  #   mount Sidekiq::Web => '/admin/sidekiq'
  # end

  get 'api/products', to: 'api#products'
  get 'api/categories', to: 'api#categories'
  get 'api/categories_fields', to: 'api#categories_fields'
  get 'api/categories_fields_values', to: 'api#categories_fields_values'
  get 'api/products_fields_values', to: 'api#products_fields_values'
  get 'api/parts', to: 'api#parts'
  get 'api/vendors', to: 'api#vendors'


  # get 'products/search', :as => 'search'
  get 'pages/calculator', :as => 'calculator'
  get 'products/fetch_fields', :as => 'fetch_fields'
  get 'products/fetch_field_type', :as => 'fetch_field_type'
  get 'products/conversion_of_currency', :as => 'conversion_of_currency'
  get 'products/update_categories', :as => 'update_categories'
  get 'products/fetch_vendors', :as => 'fetch_vendors'
  get 'products/fetch_product_fields', :as => 'fetch_product_fields'
  get 'products/fetch_fields_sizes', :as => 'fetch_fields_sizes'
  resources :products
  resources :cars
  root 'products#index'
  resources :news_items, path: 'news', only: [:index, :show]
  resources :offer_items, path: 'offers', only: [:index, :show]
  get '/contacts', to: 'pages#contacts'
  get '/about', to: 'pages#about'
  get '/deliver', to: 'pages#deliver'
  get '/rules', to: 'pages#rules'
  get 'vendors/update_vendors', :as => 'update_vendors'
  get 'vendors/select_vendors', :as => 'select_vendors'
  get 'car_models/fetch_models', :as => 'fetch_models'
  get 'categories_fields_values/fetch_categories_fields_values', :as => 'fetch_categories_fields_values'
  get 'categories_fields_values/set_categories_fields_values', :as => 'set_categories_fields_values'
  get 'car_models/fetch_car_models', :as => 'fetch_car_models'
  get 'car_models/fetch_years', :as => 'fetch_years'
  get 'car_models/fetch_modifications', :as => 'fetch_modifications'
  get 'car_models/fetch_sizes', :as => 'fetch_sizes'
  resources :orders, only: [:update, :show, :destroy, :edit]
  resources :order_items, only: [:create, :destroy, :update]
  resources :bookmarks, only: [:update, :show, :destroy]
  resources :bookmark_items, only: [:create, :destroy]
  # resource :order, only: [:update, :show, :destroy] do
  #   resources :order_items, only: [:create, :destroy, :update]
  # end

  get 'payment/cancelled', to: 'payments#cancelled'
  post 'payment/result', to: 'payments#result'
  get 'payment/connection_failed', to: 'payments#connection_failed'

  # get "/system/*path", to: redirect('http://autoshina.kg/system/%{path}.%{format}') if Rails.env.development? && !ENV['ASSETS_HOST']
end
