module AppConstants
  # Maximum tires in one order
  MAXIMUM_TIRES = 8

  # set constants for card payment
  CURRENCY = 417
  LANG = 'ru'

  # The name of a file which stores PKey and Certificate issued by IPC
  SSL_KEYSTORE_NAME = 'imakstore.p12'

  # The name of a file containing a PEM formatted certificate
  SSL_CERT_NAME = 'MCertResp.pem'

  # The name of a file containing a private SSL key
  IMA_KEY_NAME = 'ima-p.key'
end
