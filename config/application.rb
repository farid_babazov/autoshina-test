require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module AutoshinaKg
  class Application < Rails::Application

    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.assets.precompile += %w( application-ie.css )
    config.assets.precompile += %w( .svg .eot .woff .ttf)
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    config.autoload_paths << Rails.root.join('lib')
    config.assets.precompile += Ckeditor.assets
    config.assets.precompile += %w(ckeditor/*)
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.time_zone = 'Almaty'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.before_configuration do
      I18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}').to_s]
      I18n.locale = :ru
      I18n.default_locale = :ru
      config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}').to_s]
      config.i18n.locale = :ru
      # bypasses rails bug with i18n in production\
      I18n.reload!
      config.i18n.reload!
    end
    config.i18n.default_locale = :ru
    config.locale = :ru

    config.action_mailer.default_url_options = {:host => ENV['DOMAIN'], :from => ENV['WEBSITE_OUTGOING_EMAIL']}
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      :address   => ENV['SMTP_SERVER'],
      :port      => ENV['SMTP_PORT'],
      :domain => ENV['DOMAIN'],
      :authentication => :plain,
      :enable_starttls_auto => true,
      :user_name => ENV['WEBSITE_OUTGOING_EMAIL'],
      :password  => ENV['EMAIL_PASSWORD']
    }
  end
end
