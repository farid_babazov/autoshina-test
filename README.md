== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

Project requires 2.0.0-p353
But in case it can't be installed for some reason, can be substituted by 2.2.1

* System dependencies

If some problems occur with pry-debugger during bundle, it can be just commented out

* Configuration

* Database creation

We couldn't replicate db creation, so we just took a db dump from production

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

Prior to deploy, the developer has to
* exchange ssh keys with bitbucket (ex. push without entering password)
* have enough rights in the project repo (write or admin)

In order to deploy,
* the changes have to be pushed to repo in mater branch
* from a terminal inside project root directory, run

$ cap deploy



* ...

Additional info after integration of Elcart Payment
* you need to run rake tasks to populate the database from csv files:
** with Delivery cost - rake db:delivery_cost RAILS_ENV=production
** with Card Payment Errors - rake db:card_payment_errors RAILS_ENV=production

Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
