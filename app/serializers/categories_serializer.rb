class CategoriesSerializer < ActiveModel::Serializer

  attributes  :id,
              :name, 
              :image, 
              :part_id, 
              :remote_id

  def image
    "http://autoshina.kg" + object.image.url

  end

end
