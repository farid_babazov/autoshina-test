class ProductsSerializer < ActiveModel::Serializer

  attributes  :id,
              :name, 
              :remote_id, 
              :internal_name, 
              :image, 
              :cost, 
              :count, 
              :vendor_id, 
              :category_id, 
              :part_id, 
              :discount

  def image
      # binding.pry
    object.image_url(:show)

  end

end
