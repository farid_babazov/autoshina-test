class PartsSerializer < ActiveModel::Serializer

  attributes  :id,
              :name, 
              :categories_name

end
