class ProductsFieldsValuesSerializer < ActiveModel::Serializer

  attributes  :id,
              :product_id, 
              :categories_field_id, 
              :value

end
