class VendorsSerializer < ActiveModel::Serializer

  attributes  :id,
              :name, 
              :category_id, 
              :image, 
              :remote_id

  def image
    "http://autoshina.kg" + object.image.url
  end

end
