class CategoriesFieldsSerializer < ActiveModel::Serializer

  attributes  :id,
              :remote_id,
              :category_id, 
              :type_field, 
              :search_type 

end
