class CategoriesFieldsValuesSerializer < ActiveModel::Serializer

  attributes  :id,
              :remote_id, 
              :categories_field_id, 
              :value

end
