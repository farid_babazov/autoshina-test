class EmailCustomerJob
  include SuckerPunch::Job
  workers 4

  def perform(order_id)
    ActiveRecord::Base.connection_pool.with_connection do
      order = Order.find(order_id)
      OrderMailer.customer_order(order).deliver
    end
  end
end
