class CleanupOrdersJob
  include SuckerPunch::Job

  def perform
    # message = Message.find(message)
    # NotificationsMailer.delay.new_message(message)
    ActiveRecord::Base.connection_pool.with_connection do
      Order.cleanup!
    end
  end
end
