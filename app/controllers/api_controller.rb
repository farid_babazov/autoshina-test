class ApiController < ActionController::Base
  respond_to :json

  def products
    #http://localhost:3000/api/products?limit=3&offset=1
    @products = Product.price_exists.offset(params[:offset]).limit(params[:limit])
    respond_with @products, each_serializer: ProductsSerializer
  end


  def categories
    @categories = Category.all
    respond_with @categories, each_serializer: CategoriesSerializer
  end

  def categories_fields
    @categories_fields = CategoriesField.all
    respond_with @categories_fields, each_serializer: CategoriesFieldsSerializer
  end

  def categories_fields_values
    @categories_fields_values = CategoriesFieldsValue.all
    respond_with @categories_fields_values, each_serializer: CategoriesFieldsValuesSerializer
  end

  def products_fields_values
    @products_fields_values = ProductFieldsValue.all
    respond_with @products_fields_values, each_serializer: ProductsFieldsValuesSerializer
  end

  def parts
    @parts = Part.all
    respond_with @parts, each_serializer: PartsSerializer
  end


  def vendors
    @vendors = Vendor.all
    respond_with @vendors, each_serializer: VendorsSerializer
  end
end
