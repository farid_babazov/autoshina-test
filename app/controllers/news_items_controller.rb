class NewsItemsController < ApplicationController

  def index
    @page_name = "Новости"
    @news_items = NewsItem.all.page(params[:page])
  end

  def show
    @page_name = "Новости"
    @news_item = NewsItem.find params[:id]
  end
end
