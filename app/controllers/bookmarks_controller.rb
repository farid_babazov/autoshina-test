class BookmarksController < ApplicationController

  def show
  end


  def destroy
    if @bookmark.bookmark_items.destroy_all 
      render :empty_bookmark
    else
      render nothing: true, status: 405
    end
  end

end