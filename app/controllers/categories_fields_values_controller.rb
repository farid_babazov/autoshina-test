class CategoriesFieldsValuesController < ApplicationController


  def fetch_categories_fields_values
    # binding.pry
    category = Category.find params[:category_id]
    @fields = CategoriesField.where(category_id: category).map { |c| [c.field_name, c.id]}
  end


  def set_categories_fields_values
    # binding.pry
    # category = Category.find params[:category_id]
    # @cf = CategoriesField.where(category_id: category).map { |c| [c.field_name, c.id]}

    category = CategoriesField.find(params[:category_field_id]).category
    @category = CategoriesField.find(params[:category_field_id]).category.id
    @fields = category.categories_fields.map { |c| [c.field_name, c.id]}
  end
end
