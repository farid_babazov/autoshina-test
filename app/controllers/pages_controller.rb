class PagesController < ApplicationController
  
  def about
    @page_name = "О нас"
    @about = StaticPage.find(2)
    @about_site = StaticPage.find(3)
  end

  def index
  end

  def contacts
    @page_name = "Контакты"
    @contact = StaticPage.find(5)
    @map1 = StaticPage.find(6)
    @map2 = StaticPage.find(7)
    @map3 = StaticPage.find(8)
    @map4 = StaticPage.find(9)
    @map5 = StaticPage.find(10)
    @map6 = StaticPage.find(11)
  end

  def deliver
    @page_name = "Доставка"
    @page = StaticPage.find(4)
    @featured = Product.price_exists.featured.limit(15)
  end

  def rules
    @page = StaticPage.find(1)
    
  end

  def calculator
    @page_name = "Калькулятор"
    @width = (125..355).step(10).to_a
    @width.insert(0, ["Ширина шины", 175])
    @height = (25..85).step(5).to_a
    @height.insert(0, ["Высота шины", 70])
    @diameter = (12..24).to_a
    @diameter.insert(0, ["Диаметр диска", 13])

    @height_i = [27,29,30,31,32,33,35,37,38,40]
    @height_i.insert(0, ["Диаметр шины", 31])

    @width_i = (8.5..15.5).step(1).to_a
    @width_i.insert(0, ["Ширина шины", 10.5])
    
    @diameter_i = [14,15,16,16.5,17,18,20,22]
    @diameter_i.insert(0, ["Диаметр диска", 15])
  end
end
