class BookmarkItemsController < ApplicationController

  before_action :fetch_bookmark_item, only: [:destroy]

  def create
    product = Product.find bookmark_item_params[:product_id]
    @bookmark_item = @bookmark.bookmark_items.find_or_initialize_by product_id: product.id
    # @bookmark_item.increment :quantity, order_item_params.fetch(:quantity, 1).to_i
    @bookmark_item.save
    respond_to do |format|
      format.html { redirect_to :back }
      format.js { :update_bookmark }
    end
  end

  def destroy
    # binding.pry
    @bookmark_item.destroy
    respond_to do |format|
      format.html { redirect_to :back }
      format.js { :destroy }
    end
  end

  def update
    order_item_params = order_item_update_params
    if order_item_params[:quantity].to_i > 0
      @order_item.update order_item_params
    else
      @order_item.destroy
    end
    respond_to do |format|
      format.html { redirect_to :back }
      format.js { render :update_basket }
    end
  end

  # protected

  # def update_cart_or_basket
  #   if request.referer =~ /\/cart\/?/ # if requested from cart purchasing page
  #     render :update_basket # update cart purchasing view
  #   else
  #     render :update_cart # update sidebar cart
  #   end
  # end

  def bookmark_item_params
    params.require(:bookmark_item).permit(:product_id)
  end

  def fetch_bookmark_item
    @bookmark_item = @bookmark.bookmark_items.find(params[:id])
  end

  def order_item_update_params
    params.require(:order_item).permit(:quantity)
  end
end