class ProductsController < ApplicationController
    before_action :check_params, only: :fetch_fields
    # before_action :search_values, :vendor, only: :index
    helper_method :sort_column, :sort_direction


  def index
    if params[:model_search]
      @products = Product.where(internal_name: params[:model_search][:size]).price_exists.page(params[:page]).order(sort_column + " " + sort_direction)
    else
      @products = Product.price_exists.search(search_values, query).page(params[:page]).order(sort_column + " " + sort_direction)
    end   

    @featured = Product.featured.where(part_id: 1).limit(25)
  end

  def show
    @product = Product.find(params[:id])
    respond_to do |format|
      format.html do
        @product = Product.find(params[:id])
        @product.increment :featured, 1
        @product.save
      end
      format.js do
        @product = Product.find(params[:id])
      end
    end 
  end

  def fetch_fields
    @fields = Category.find(params[:category_id]).categories_fields.where(show_in_browse: "1") unless params[:category_id].nil?
  end

  def conversion_of_currency
    if cookies[:currency] == "dollar"
      cookies[:currency] = "som"
      redirect_to :back
    else
      cookies[:currency] = "dollar"
      redirect_to :back
    end
  end

  def update_categories
    unless params[:part_id].blank?
      part = Part.find(params[:part_id])
      @categories = part.categories.map{|a| [a.name, a.id]}.unshift ["Выберите категорию", ""]
    else
      @categories = [["",""]]
    end
  end

  def fetch_vendors
    unless params[:category_id].blank?
      category = Category.find(params[:category_id])
      @vendors = category.vendors.map{|a| [a.name, a.id]}.unshift ["Выберите производителя", ""]
    else
      @vendors = [["",""]]
    end
  end

  def fetch_product_fields
    # binding.pry
    sleep 0.2
    category = Category.find(params[:category_id])
    @categories_fields = category.categories_fields.map{|a| [a.field_name, a.id]}
  end

  def fetch_field_type
    field = CategoriesField.find params[:category_field_id]
    if field.type_field == "html_text_area"
      render json: {}, status: 200
    else
      render json: {}, status: 504
    end
  end

  private
    def check_params
      unless params[:category_id].present?
        render nothing: true
        return false 
      end
    end



    def search_values
      season_vars = Array.new
      # binding.pry
      if params[:search_size]
        search_size = Array.new
        params[:search_size].each do |key, value|
          search_size.push value if value.present? and key =~ /^field_[0-9]/
          # values.push key.split("field_").last if value.present? and key =~ /^field_[a-я]/
        end
      end
      if params[:search_season]
        search_season = params[:search_season].keys
      end
      if search_season.present? and search_size.present?
        return season_vars = search_season.map{|s| [s] + search_size }
      end
      if search_season.nil?
        return season_vars << search_size
      elsif search_size.nil?
        return season_vars << search_season
      elsif search_season and search_size.empty?
        return season_vars = search_season.map{|s| [s]}
      end
    end

    def query
      query = String.new
      if params[:search_type]
        query = "#{"part_id = #{params[:search_type][:part_id]}"}" if params[:search_type][:part_id].present?
        query = "#{"category_id = #{params[:search_type][:category_id]}"}" if params[:search_type][:category_id].present?
        query = "#{"vendor_id = #{params[:search_type][:vendor_id]}"}" if params[:search_type][:vendor_id].present?
      end
      query
    end

    def sort_column
      Product.column_names.include?(params[:sort]) ? params[:sort] : "name"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

end