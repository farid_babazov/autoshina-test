class OrdersController < ApplicationController

  def show
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    if @order.update order_params
      @order.update_attributes delivery_cost: @order.delivery.cost
      @order.update_attributes delivery_region: @order.delivery.region

      send_order_for_payment
    else
      render :edit
    end
  end

  def destroy
    if @order.order_items.destroy_all
      render :empty_cart
    else
      render nothing: true, status: 405
    end
  end

  private

  def order_params
    params.require(:order).permit :name, :address, :phone, :email, :delivery_id, :payment_type
  end

  def send_order_for_payment
    order_params[:payment_type] == 'cash' ? cash_payment : pay_with_card
  end

  def cash_payment
    @order.update_attributes status: "confirmed", order_date: DateTime.now
    order = Order.create!
    session[:order_id] = order.id
    featured_products = FeatureProduct.new(@order)
    featured_products.perform

    EmailNewOrderJob.perform_async(@order.id)
    #EmailCustomerJob.perform_in(5, @order.id)
    # render js: "window.location.pathname='#{URI(request.referer).path}'"
    redirect_to root_path
    # render :complete
    flash[:notice] = "Ваш заказ принят! В ближайшее время с Вами свяжутся"
  end

  def pay_with_card
    threed_secure = UseThreedSecure.new
    post_msg = set_payment_post_details

    transaction_id = threed_secure.get_transaction_ID(post_msg)

    if transaction_id == 'No response' || !transaction_id
      redirect_to payment_connection_failed_path
    else
      @order.create_payment(transaction_id: transaction_id)
      session[:transaction_id] = transaction_id

      redirect_to threed_secure.get_redirect_URL(transaction_id)
    end
  end

  def set_payment_post_details
    to_post = {}

    total_price = @order.total_discounted_price_som
    total_shipping = @order.delivery.cost

    total = (total_price + total_shipping).round(2)

    product_description = 'Оплата за : ' + @order.order_items.first.product_name

    to_post = {
      amount: (total * 100).ceil,
      currency: AppConstants::CURRENCY,
      desc: product_description,
      language: AppConstants::LANG,
      client_ip_addr: request.ip
    }

    to_post
  end
end
