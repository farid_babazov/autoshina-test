class CarModelsController < ApplicationController

  def fetch_models
    @models = (CarManufacturer.find(params[:manufacturer]).car_models.order(:model).map { |c| [c.model, c.id] }).insert(0, ["Модель", ""])
  end

  def fetch_car_models
    unless params[:manufacturer] == ""
      @models = (CarManufacturer.find(params[:manufacturer]).car_models.order(:model).map { |c| [c.model, c.id] })
    else
      @models = [["",""]]
    end
  end

  def fetch_years
    # binding.pry
    @years = (CarModel.find(params[:model]).car_modifications.order(:year).map { |c| [c.year, c.year] }).insert(0, ["Год выпуска", ""]).uniq
  end

  def fetch_modifications
    @modifications = (CarModel.find(params[:model]).car_modifications.where(year: params[:year]).order(:modification).map { |c| [c.modification, c.id] }).insert(0, ["Модификация", ""])
  end

  def fetch_sizes
    size = CarModification.find params[:modification]
    @default = size.default.split(/[|#]/) if size.default
    @replacement = size.replacement.split(/[|#]/) if size.replacement
    @tuning = size.tuning.split(/[|#]/) if size.tuning
  end
end
