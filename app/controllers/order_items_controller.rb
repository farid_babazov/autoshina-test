class OrderItemsController < ApplicationController

  before_action :fetch_order_item, only: [:update, :destroy]

  def create
    product = Product.find order_item_params[:product_id]

    if is_deliverable?(product)
      @order_item = @order.order_items.find_or_initialize_by product_id: product.id
      @order_item.increment :quantity, order_item_params.fetch(:quantity, 1).to_i
      @order_item.save
      get_updated_order
      @msg = 'Товар добавленен в корзину'
    else
      max_tire_warning_msg
    end
    respond_to do |format|
      format.html { redirect_to :back }
      format.js { :update_basket }
    end
  end

  def destroy
    @order_item.destroy
    respond_to do |format|
      format.html { redirect_to :back }
      format.js { :update_cart }
    end
  end

  def update
    order_item_params = order_item_update_params
    if order_item_params[:quantity].to_i > 0
      get_updated_basket
    else
      @order_item.destroy
    end

    get_updated_order()
    respond_to do |format|
      format.html { redirect_to :back }
      format.js { render :update_basket }
    end
  end

  # protected

  # def update_cart_or_basket
  #   if request.referer =~ /\/cart\/?/ # if requested from cart purchasing page
  #     render :update_basket # update cart purchasing view
  #   else
  #     render :update_cart # update sidebar cart
  #   end
  # end
  private

  def order_item_params
    params.require(:order_item).permit(:product_id, :quantity)
  end

  def fetch_order_item
    @order_item = @order.order_items.find(params[:id])
  end

  def order_item_update_params
    params.require(:order_item).permit(:quantity)
  end

  def get_updated_order
    @order = Order.find(session[:order_id])
  end

  def get_updated_basket
    if is_deliverable?(@order_item.product)
      @order_item.update order_item_params
      @msg = "Обновлено количество товаров в корзине"
    else
      max_tire_warning_msg
    end
  end

  def max_tire_warning_msg
    @msg = "В одном заказе максимум #{AppConstants::MAXIMUM_TIRES} шин !"
  end

  def is_deliverable?(product)
    return true unless product.is_tire?
    total_tires <= AppConstants::MAXIMUM_TIRES ? true : false
  end

  def total_tires
    total_number = order_item_params[:quantity].to_i
    items = @order.order_items
    items.map{ |item| total_number += item.quantity.to_i if item.is_tire? }
    total_number -= @order_item.quantity if params[:action] == 'update'
    total_number
  end
end
