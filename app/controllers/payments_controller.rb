class PaymentsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:result]

  def new
  end

  def cancelled
  end

  def connection_failed
    @response = {
        status: 'failed',
        title: "Платеж не прошел",
        description: "Услуга оплаты картой временно недоступна.",
        code: "No response"
    }

    render :result
  end

  def result
    transaction_result = get_payment_result

    result_code = transaction_result[:result_code]
    @response = build_response_to_display(result_code)
    finish_card_payment(transaction_result)
  end

  private

  def get_payment_result
    threed_secure = UseThreedSecure.new
    threed_secure.get_transaction_info(set_post_request)
  end

  def set_post_request
    { trans_id: session[:transaction_id],
      client_ip_addr: request.ip }
  end

  def build_response_to_display(result_code)
    response = {}
    if result_code == '000'
      response = {
        status: 'success',
        title: 'Платеж проведен успешно',
        description: 'Вы будете перенаправленны на главную страницу через'
      }
    elsif result_code == CardPaymentError.code(result_code)
      response = {
        status: 'failed',
        title: code.title,
        description: code.description
      }
    else
      response = {
        status: 'failed',
        title: "Платеж не прошел",
        description: "Для уточнения причины отклонения платежа вам нужно связаться с банком, выпустившим вашу платежную карту."
      }
    end
    response.merge!(code: result_code)
  end

  def finish_card_payment(transaction_result)
    @order = Order.find(session[:order_id])
    if transaction_result[:result_code] == '000'
      @order.update_attributes status: "confirmed", order_date: DateTime.now
      EmailNewOrderJob.perform_async(@order.id)
      EmailCustomerJob.perform_in(5, @order.id)
      featured_products = FeatureProduct.new(@order)
      featured_products.perform
    else
      @order.update_attributes status: "canceled", order_date: DateTime.now
    end
    @order.payment.update_attributes(
      approval_code: transaction_result[:approval_code],
      result: transaction_result[:result],
      result_code: transaction_result[:result_code],
      rrn: transaction_result[:rrn]
      )

    order = Order.create!
    session[:order_id] = order.id
  end
end
