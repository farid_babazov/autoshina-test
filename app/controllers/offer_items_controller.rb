class OfferItemsController < ApplicationController
  def index
    @page_name = "Акции"
    @offer_items = OfferItem.all.page(params[:page])
  end

  def show
    @page_name = "Акции"
    @offer_item = OfferItem.find params[:id]
  end
end
