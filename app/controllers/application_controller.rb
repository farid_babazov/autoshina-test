class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :define_products_search
  before_action :fetch_order
  before_action :fetch_bookmark
  before_action :default_currency
  before_action :site_settings

  def define_products_search
    # @search = Product.index.search params[:q]
    @search = Product.search(params[:q])
    @search_cars = Car.search(params[:q])
  end

  def fetch_order
    order_id = session[:order_id]
    @order = Order.find_by(id: order_id) || Order.create
    session[:order_id] = @order.id
  end

  def default_currency
    # session[:currency] |= "som"
    currency = cookies[:currency]
    cookies[:currency] = currency || "som"
  end

  def fetch_bookmark
    bookmark_id = session[:bookmark_id]
    @bookmark = Bookmark.find_by(id: bookmark_id) || Bookmark.create
    session[:bookmark_id] = @bookmark.id
  end

  def site_settings
    @site_settings = SiteSetting.find(1)
    @phones = @site_settings.phone.scan(/\d{2}[\s\d-]+/)
    # @phones = /\d{2}[\s\d-]+/.match(@site_settings.phone).matches.to_a.to_s
  end
end
