ActiveAdmin.register_page "Dashboard" do

  menu label: 'Сводка', priority: 1

  content title: "Сводка" do
  
    panel 'Товары' do
      ul do
        li link_to "Общее количество товаров: #{Product.count}", admin_products_path
        li link_to "Добавить еще", new_admin_product_path
      end
    end

    panel 'Стек невыполненных заказов' do
      ol do
        Order.where(status: 'confirmed').limit(20).map do |order|
          price = number_to_currency order.total_price, format: '%n %u', unit: "сом"
          ue = number_to_currency order.total_price/SiteSetting.find(1).current_rate, format: '%n%u', unit: "$"
            li link_to "#{I18n.l(order.order_date, format: :dashboard)} на #{price}(#{ue})", edit_admin_order_path(order)
        end
      end
    end


    # panel 'Заказы в процессе выполнения' do
    #   ol do
    #     Order.where(status: 2..3).limit(20).map do |order|
    #       price = number_to_currency order.total_price
    #       li link_to "#{I18n.l(order.order_date, format: :dashboard)} на #{price}", edit_admin_order_path(order)
    #     end
    #   end
    # end

    panel 'Стек выполненных заказов' do
      ol do
        Order.where(status: "delivered").order(updated_at: :desc).limit(20).map do |order|
          price = number_to_currency order.total_price, format: '%n %u', unit: "сом"
          ue = number_to_currency order.total_price/SiteSetting.find(1).current_rate, format: '%n%u', unit: "$"
            li link_to "#{I18n.l(order.order_date, format: :dashboard)} на #{price}(#{ue})", edit_admin_order_path(order)
        end
      end
    end

  end
end
