ActiveAdmin.register NewsItem do
config.filters = false
  permit_params :title, :text, :image, :snippet
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  form do |f|
    f.inputs do
      f.input :title
      f.input :snippet
      f.input :image, as: :file
      f.input :text, :as => :ckeditor, :input_html => { :ckeditor => {toolbar: 'mini', height: "600",:customConfig => '/assets/active_admin/ckeditor/config.js'} }
    end

    f.actions
  end

   show do |news|
    attributes_table do
      row :image do
        image_tag news.image.thumb.url
      end
      row :title
      row :snippet
      row :created_at
      row :text, class: "news_text" do |x|
        x.text.html_safe
      end
    end
    active_admin_comments
  end

  index do
   selectable_column
    column :title
    column :snippet
    column :created_at
    actions
  end
end
