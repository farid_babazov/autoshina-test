ActiveAdmin.register CarModification do

  permit_params :car_manufacturer_id, :car_model_id, :year, :modification, :default, :replacement, :tuning
  actions :all, except: [:show]
  menu :parent => "Авто", priority: 3
  form do |f|
    f.inputs do
      f.input :car_manufacturer, as: :select, collection:  CarManufacturer.all
      f.input :car_model, as: :select, collection:  (CarModel.where(car_manufacturer_id: f.object[:car_manufacturer_id]).map { |c| [c.model, c.id] })
      f.input :year
      f.input :modification
      f.input :default
      f.input :replacement
      f.input :tuning
    end

    f.actions
  end

  index do
   selectable_column
    column :car_manufacturer do |m|
      m.car_model.car_manufacturer.title
    end
    column :car_model do |m|
      m.car_model.model
    end
    column :year
    column :modification
    column :default
    column :replacement
    column :tuning
    actions
  end

end
