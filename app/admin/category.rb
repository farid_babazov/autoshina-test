ActiveAdmin.register Category do

  permit_params :category_id, :name, :image, :order, :part_id
  filter :category
  actions :all, except: [:show]

  menu :parent => "Характеристики", priority: 2
  form do |f|
    f.inputs do
      f.input :name
      f.input :part, prompt: "Выберите тип категории"
    end
    f.actions
  end

  index do
    selectable_column
    column :name
    column :part
    actions
  end
end
