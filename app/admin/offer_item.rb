ActiveAdmin.register OfferItem do
config.filters = false
  permit_params :title, :text, :image, :snippet

  form do |f|
    f.inputs do
      f.input :title
      f.input :snippet
      f.input :image, as: :file
      f.input :text, :as => :ckeditor, :input_html => { :ckeditor => {toolbar: 'mini', height: "600",:customConfig => '/assets/active_admin/ckeditor/config.js'} }
    end

    f.actions
  end

   show do |offer|
    attributes_table do
      row :image do
        image_tag offer.image.thumb.url
      end
      row :title
      row :snippet
      row :created_at
      row :text, class: "news_text" do |x|
        x.text.html_safe
      end
    end
    active_admin_comments
  end

  index do
   selectable_column
    column :title
    column :snippet
    column :created_at
    actions
  end
end
