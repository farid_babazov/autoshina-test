ActiveAdmin.register Price do
  permit_params :file

  actions :all, except: [:edit, :update, :show]


  form do |f|
    f.inputs do
      f.input :file, as: :file
    end
    f.actions
  end

  index do
   selectable_column
    column :file do |xls|
      xls.file.file.filename
    end
    column :created_at
    actions
  end

end
