ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation
  actions :all, except: [:show]
  # filter :email
  # filter :current_sign_in_at
  # filter :sign_in_count
  # filter :created_at
  config.filters = false

  index do |admin_user|
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    column "" do |user|
      if user == current_admin_user
        link_to "Редактировать", edit_admin_admin_user_path(user)
      end
    end
  end


  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
