ActiveAdmin.register Delivery do
  permit_params :region, :cost

  menu label: 'Доставка'

  filter :region
  filter :cost

  index do
    selectable_column
    column :region
    column :cost
    actions
  end
end
