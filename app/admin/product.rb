ActiveAdmin.register Product do
  permit_params :name, :internal_name, :part_id, :category_id, :vendor_id, :image, :video, :meta_keywords, :meta_description, :cost, :count, :discount, :remote_id, product_fields_values_attributes: [ :id, :value, :categories_field_id, :_destroy ]
  before_filter :only => :index do
    @per_page = SiteSetting.first.per_page || 30
  end
  # config.sort_order = "cost_asc"
  scope :all
  scope :tires
  scope :oils
  scope :accumulator
  scope :rims

  filter :name
  filter :internal_name
  filter :category
  filter :vendor
  filter :cost
  filter :count
  filter :discount


  form do |f|
    f.inputs do
      f.input :remote_id
      f.input :name
      f.input :internal_name
      f.input :part, prompt: "Выберите тип товара"
      f.input :category, prompt: "Выберите категорию товара", collection:  Category.where(part_id: f.object[:part_id] )
      f.input :vendor, prompt: "Выберите производителя", collection:  Vendor.where(category_id: f.object[:category_id] )
      f.input :image, as: :file
      f.input :video
      f.input :meta_keywords
      f.input :meta_description
      f.input :cost
      f.input :count
      f.input :discount
    end
    # f.inputs "Информация о клиенте" do
    #   f.template.render("active_admin/products/product_fields")
    # end
    f.inputs "", :id => "products_fields" do
      f.has_many :product_fields_values, :allow_destroy => true, :heading => 'Свойства',:force => true do |cf|
        cf.input :categories_field, collection:  CategoriesField.where(category_id: f.object[:category_id] ).map{ |c| [c.field_name, c.id] }
        # binding.pry

        if cf.object.categories_field.present? and cf.object.categories_field.type_field == "html_text_area"
          cf.input :value, :as => :ckeditor, :input_html => { :ckeditor => {toolbar: 'mini', height: "600",:customConfig => '/assets/active_admin/ckeditor/config.js'} }
        else
          cf.input :value
        end
      end
    end
    f.actions
  end

  show do |product|
    attributes_table do
      row :image do
        image_tag product.image.thumb.url
      end
      row :remote_id
      row :name
      row :internal_name
      row :category
      row :vendor
      row :count
      row :cost
      row :discount
      row :meta_keywords
      row :meta_description

      if product.product_fields_values.any?
        panel 'Характеристики' do
          render("active_admin/products/fields")
        end
      end
    end
    active_admin_comments
  end

  index do
    selectable_column
    column :name
    column :internal_name
    column :category, sortable: "products.category_id"
    column :vendor, sortable: "products.vendor_id"
    column :count
    column :cost, sortable: "products.cost" do |p|
      number_to_currency p.cost, format: "%n %u"
    end
    column :discount_price do |p|
      number_to_currency p.discount_price, format: "%n %u"
    end
    column :discount, sortable: "products.discount" do |p|
      p.discount.to_s+"%"
    end
    actions
  end

  # action_item do
  #   link_to "Export to Excel", root_path
  #   button_
  # end
  # member_action :export do
  #   # @order = Order.find params[:id]
  #   # render xlsx: "export", filename: "Заказ#{@order.id}", disposition: 'inline', layout: false
  # end

  sidebar I18n.t(:filter_by_fields), only: :index do
    render partial: 'search'
  end

  batch_action :update_discounts, confirm: 'Установить скидку в %', form: {discount: :text} do |selection|
    Product.find(selection).each do |order|
      order.update_attributes discount: params[:batch_action_inputs].scan(/\d/).join.to_i
    end
    redirect_to admin_products_path, notice: I18n.t("active_admin.batch_actions.update_statuses", count: selection.count)
  end

  collection_action :index, :method => :get do

  end




    controller do

      def index
        index! do |format|
          if params[:field_value].present? and params[:q].blank?
            # binding.pry
            values = params[:field_value].split(" ")
            conditions = "(value IN (" + values.map{|v| "'#{v}'" }.join(", ") + "))"
            @products= Product.joins(:product_fields_values).merge(ProductFieldsValue.where(conditions)).group('products.id').select("products.*").having('COUNT(*) = ?', values.size).page(params[:page])
            # @products = Product.joins(:product_fields_values).where("value ILIKE '#{params[:field_value]}'").page(params[:page])
          end
          format.html
        end
      end

    end

end
