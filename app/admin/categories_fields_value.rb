ActiveAdmin.register CategoriesFieldsValue do

  permit_params :categories_field_id, :value
  actions :all, except: [:show]
  menu :parent => "Характеристики", priority: 3

  filter :value
  filter :categories_field, as: :select, collection: CategoriesField.all.map { |c| [c.field_name, c.id] }
  filter :categories_field_category_id, as: :select, collection: Category.all

  index do
    selectable_column
    column :category do |c|
      c.categories_field.category.name
    end
    column :categories_field do |c|
      c.categories_field.field_name
    end
    column :value
    actions
  end

  form do |f|
    f.inputs  do
      f.input :remote_id, as: :select, collection: Category.all,:input_html => {  id: "cfvc" }
      f.input :categories_field, as: :select, collection: CategoriesField.all.map { |c| [c.field_name, c.id] }
      f.input :value
    end

    f.actions
  end

end
