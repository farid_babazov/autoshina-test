ActiveAdmin.register Payment do
  actions :index, :destroy

  menu label: 'Платежи'

  filter :order_id
  filter :transaction_id
  filter :rrn
  filter :approval_code
  filter :result

  index do
    selectable_column
    column 'Заказ' do |payment|
      link_to payment.order_id, admin_order_path(payment.order_id) if payment.order_id.present?
    end
    column :transaction_id
    column :result
    column :result_code
    column 'RRN', :rrn
    column :approval_code
    column :updated_at
  end
end
