ActiveAdmin.register CarModel do

  permit_params :model, :car_manufacturer_id
  actions :all, except: [:show]
  filter :car_manufacturer
  filter :model
  menu :parent => "Авто", priority: 2
  form do |f|
    f.inputs do
      f.input :car_manufacturer
      f.input :model
    end

    f.actions
  end

  index do
   selectable_column
    column :car_manufacturer do |c|
      c.car_manufacturer.title
    end
    column :model
    actions
  end
end
