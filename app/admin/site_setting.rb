ActiveAdmin.register SiteSetting do

  permit_params :current_rate, :site_name, :meta_description, :meta_keywords, :phone, :email, :per_page
  menu :label => "Общие настройки", url: "/admin/site_settings/1/edit"
  actions :all, only: [ :edit]
  
  # menu :parent => "Категории"

  controller do
    def index
      redirect_to "/admin/site_settings/1/edit"
    end
    def show
      redirect_to "/admin/site_settings/1/edit"
    end
  end
  
end
