ActiveAdmin.register CarManufacturer do

  menu :parent => "Авто", priority: 1
  permit_params :title
  actions :all, except: [:show]
  config.filters = false
  form do |f|
    f.inputs do
      f.input :title
    end

    f.actions
  end

  show do |news|
    attributes_table do
      row :title
    end
    active_admin_comments
  end

  index do
   selectable_column
    column :title
    actions
  end

end
