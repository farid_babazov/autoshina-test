ActiveAdmin.register StaticPage do
  permit_params :title, :content

  actions :all, except: [:new, :create, :destroy]
  config.batch_actions = false
  form do |f|
    f.inputs do
      f.input :title
      f.input :content, :as => :ckeditor, :input_html => { :ckeditor => {toolbar: 'mini', height: "600",:customConfig => '/assets/active_admin/ckeditor/config.js'} }
    end

    f.actions
  end

  show do |news|
    attributes_table do
      row :title
      row :content, class: "news_content" do |x|
        x.content.html_safe
      end
    end
    active_admin_comments
  end

  index do
   selectable_column
    column :title
    column :updated_at
    actions
  end
end
