ActiveAdmin.register Order do
  actions :all, except: [:new, :create]
  permit_params :status

  filter :name
  filter :phone
  filter :address

  show do |order|
    panel 'Информация о клиенте' do
      render("active_admin/orders/client_info")
    end
    panel 'Информация о заказе' do
      render("active_admin/orders/order")
    end
    active_admin_comments
  end

  index do
    selectable_column
    column :id do |post|
      link_to post.id, admin_order_path(post)
    end
    column :name
    column :phone
    column :address
    column :order_date
    column :payment_type do |order|
      order.payment_type == 'cash' ? 'Нал.' : 'Безнал.'
    end
    column :status do |order|
      status_tag(t(order.status), order.color)
    end

    actions
  end

  form do |f|
    f.inputs "Информация о клиенте" do
      f.template.render("active_admin/orders/client_info")
    end
    f.inputs "Информация о заказе" do
      f.template.render("active_admin/orders/order")
    end
    f.inputs do
      f.input :status, as: :select, collection:  [["Подтвержден","confirmed"],["Выполнен","delivered"],["Отменен","canceled"] ], include_blank: false
    end
    f.actions
  end

  batch_action :update_statuses, confirm: 'Изменить статусы?', form: {статус: Order.order_statuses} do |selection|
    Order.find(selection).each do |order|
      order.update_attribute(:status, params[:batch_action_inputs].split("{\"статус\":\"").last.split("\"}").first)
    end
    redirect_to admin_orders_path, notice: I18n.t("active_admin.batch_actions.update_statuses", count: selection.count)
  end

  controller do
    def scoped_collection
      if params[:action] == 'index'
        Order.where.not status: "unconfirmed"
      else
        Order.all
      end
    end

    def update
      order = Order.find(params[:id])
      order.update_attribute(:status, params[:order][:status])

      redirect_to admin_order_path(order)
    end
  end
end
