ActiveAdmin.register Banner do

  permit_params :title, :file, :active, :link, :site, :order

  filter :title
  filter :site, as: :select, collection:  [["Слайдер","slider"],["Баннер","banner"]], include_blank: false
  filter :created_at
  filter :updated_at
  filter :active

  form do |f|
    f.inputs do
      f.input :title
      f.input :order
      f.input :link
      f.input :file, as: :file, hint: f.template.image_tag(f.object.file.icon.url)
      f.input :active
      f.input :site, as: :select, collection:  [["Слайдер","slider"],["Баннер","banner"],["Каталог","catalog"]], include_blank: false
    end
    f.actions
  end

  show do |banner|
    attributes_table do
      row :file do
        if MIME::Types.type_for(banner.file.path).first.content_type == "application/x-shockwave-flash"
          render "active_admin/orders/swf_file", locals: { banner: banner}
        else
          image_tag banner.file.icon.url
        end
      end
      row :title
      row :link
      row :site
      row :active do |banner|
          t(banner.active? ? :published : :not_published)
      end
    end
    active_admin_comments
  end

  index do
    selectable_column
    column :file do |banner|
      if MIME::Types.type_for(banner.file.path).first.content_type == "application/x-shockwave-flash"
        render "active_admin/orders/swf_file", locals: { banner: banner}
      else
        image_tag banner.file.icon.url
      end
    end
    column :title
    column :site
    column :active do |banner|
      t(banner.active? ? :published : :not_published)
    end

    actions
  end
end
