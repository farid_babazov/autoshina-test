ActiveAdmin.register Vendor do
  permit_params :name, :category_id, :image

  filter :name
  filter :category
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs do
      f.input :name
      f.input :category
      f.input :image, as: :file
    end
    f.actions
  end

  show do |vendor|
    attributes_table do
      row :id
      row :name
      row :category
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  index do
    selectable_column
    column :id
    column :file do |image|
      image_tag image.image.icon.url
    end
    column :name
    column :category
    actions
  end
end
