ActiveAdmin.register CategoriesField do

  permit_params :category_id, :field_name, :type_field, :search_type, :show_in_browse
  filter :category
  actions :all, except: [:show]

  menu :parent => "Характеристики", priority: 2
  form do |f|
    f.inputs do
      f.input :category
      f.input :field_name
      f.input :show_in_browse, as: :select, collection:  [["Неотображать","0"],["Отображать","1"]], include_blank: false
      f.input :type_field, as: :select, collection:  [["select","select"],["text","text"],["checkbox","checkbox"],["text_editor","html_text_area"]], include_blank: false
      f.input :search_type, as: :select, collection:  [["select","select"],["text","text"],["checkbox","checkbox"]], include_blank: false
    end
    # f.inputs "Информация о клиенте" do
    #   f.template.render("active_admin/products/product_fields")
    # end
    f.actions
  end

  index do
    selectable_column
    column :category
    column :field_name
    # column :search_type
    column :type_field
    actions
  end

  # controller do
  #   def create
  #     binding.pry
  #   end
  # end
end
