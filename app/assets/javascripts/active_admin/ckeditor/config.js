// CKEDITOR.editorConfig = function( config )
// {
//     config.language = 'ru';
//     config.filebrowserBrowseUrl = "/ckeditor/attachment_files";

//     // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Flash dialog.
//     config.filebrowserFlashBrowseUrl = "/ckeditor/attachment_files";

//     // The location of a script that handles file uploads in the Flash dialog.
//     config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";

//     // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Link tab of Image dialog.
//     config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";

//     // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Image dialog.
//     config.filebrowserImageBrowseUrl = "/ckeditor/pictures";

//     // The location of a script that handles file uploads in the Image dialog.
//     config.filebrowserImageUploadUrl = "/ckeditor/pictures";

//     // The location of a script that handles file uploads.
//     config.filebrowserUploadUrl = "/ckeditor/attachment_files";
//     config.toolbar_mini =
//     [
//         { name: 'document', items : [ 'NewPage','Preview' ] },
//         { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
//         { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'
//                  ,'Iframe' ] },
//         { name: 'links', items : [ 'Link','Unlink'] },
//         { name: 'tools', items : [ 'Maximize','-'] },
//                 '/',
//         { name: 'styles', items : [ 'Styles','Format' ] },
//         { name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
//         { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
//     ];
// };
