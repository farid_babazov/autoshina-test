$(document).ready(function() {
  if( $('#product_part_id :selected').val() == ""){
    $('#product_category_input ').hide()
  }
  if( $('#product_category_id :selected').val() == ""){
    $('#product_vendor_input ').hide();
  }

  // $('#product_part_id').change(function() {
  //   $('#product_category_id').empty();
  //   $('#product_category_input ').hide();
  //   $('#product_vendor_id').empty();
  //   $('#product_vendor_input ').hide();
  //   $.ajax({
  //     method: 'GET',
  //     dataType: "script",
  //     url: "/products/update_categories",
  //     data: ({part_id : $('#product_part_id').val()})
  //   });
  //   $('#product_category_input').show();
  //   if( $('#product_part_id :selected').val() == ""){
  //     $('#product_category_input ').hide();
  //   }
  //   $(".has_many_fields").remove();
  // });

  $(document).on("change", "#product_part_id", function (e) {
    $('#product_category_id').empty();
    $('#product_category_input ').hide();
    $('#product_vendor_id').empty();
    $('#product_vendor_input ').hide();
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/products/update_categories",
      data: ({part_id : $('#product_part_id').val()})
    });
    $('#product_category_input').show();
    if( $('#product_part_id :selected').val() == ""){
      $('#product_category_input ').hide();
    }
    $(".has_many_fields").remove();
  });

  $(document).on("change", "#product_category_id", function (e) {
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/products/fetch_vendors",
      data: ({category_id : $('#product_category_id').val()})
    });
    $('#product_vendor_input ').show();
    $("#products_fields").show()
    $(".has_many_fields").remove();
  });

  if ($('#product_category_id').val()==""){
    $("#products_fields").hide()
  }


  // $('#subsubcategory_category_id').change(function() {
  //   $.ajax({
  //     method: 'GET',
  //     dataType: "script",
  //     url: "/products/update_categories",
  //     data: ({category_id : $('#subsubcategory_category_id').val()})
  //   });
  // });


  $('#product_category_id').change(function() {
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/products/fetch_product_fields",
      data: ({category_id : $('#product_category_id').val()})
    });
    
  });

  $('.has_many_add').on('click', function(){
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/products/fetch_product_fields",
      data: ({category_id : $('#product_category_id').val()})
    });
  });



  $(document).on("change", ".has_many_fields select", function (e) {
    console.log($(this).parent().parent().find("textarea"))
    select_id = $(this).attr("id")
    textarea_id = $(this).parent().parent().find("textarea").attr("id")
    
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/products/fetch_field_type",
      data: ({category_field_id : $(this).val()}),
      success: function(msg){
        CKEDITOR.replace(textarea_id)
      },
      error: function(msg){
        t = CKEDITOR.instances[textarea_id]
        t.destroy(true)
      }
    });
  });
  

  $('#car_modification_car_manufacturer_id').change(function() {

    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/car_models/fetch_car_models",
      data: ({manufacturer : $(this).val()})
    });
  });

  set_vendors()
  function set_vendors(){
    val = $("#q_vendor_id").val()
    console.log(val)
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/vendors/select_vendors",
      data: ({category_id : $('#q_category_id').val(), }),
      complete: function(msg){
        $("#q_vendor_id [value='" + val + "']").attr("selected", "selected");
        console.log(val)
        }
    });
  }
  set_category_fields()
  function set_category_fields(){
    val = $("#categories_fields_value_categories_field_id").val()
    console.log(val)
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/categories_fields_values/set_categories_fields_values",
      data: ({category_field_id : val, }),
      complete: function(msg){
        $("#categories_fields_value_categories_field_id [value='" + val + "']").attr("selected", "selected");
        console.log(val)
        }
    });
  }

  $('#q_category_id').change(function() {
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/vendors/select_vendors",
      data: ({category_id : $(this).val()})
    });
  });

  $('#cfvc').change(function() {
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/categories_fields_values/fetch_categories_fields_values",
      data: ({category_id : $(this).val()})
    });
  });

});
