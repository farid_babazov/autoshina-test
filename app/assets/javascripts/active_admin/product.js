$(document).ready(function() {


  $(document).on("change", "#filter_category_id", function (e) {
    
    $.ajax({
      method: 'GET',
      dataType: "script",
      url: "/products/fetch_fields_sizes",
      data: ({category_id : $("#filter_category_id").val()})
    });
  });
});
