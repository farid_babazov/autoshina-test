$(document).ready(function(){  
  $(function(){
    $("#slides").slidesjs({
      width: 1280,
      height: 325,
      start: 1,
      navigation: true,
      play: {
        active: false,
        effect: "slide",
        interval: 5000,
        auto: true,
        swap: true,
        pauseOnHover: false,
        restartDelay: 2500
      },
      callback: {
        loaded: function(number) {
          
          left_position = ($("#slides").width() / 2) - ($(".slidesjs-pagination").width() / 2 );
          $(".slidesjs-pagination").css("left", left_position);

          $(".filter").stickyfloat( {duration: 400} );
        }
      }
    });
  });
  $(".slider_box").slidesjs({
      pagination: false,
      navigation: false,
      play: {
        active: false,
        effect: "slide",
        interval: 15000,
        auto: true,
        swap: true,
        pauseOnHover: false,
        restartDelay: 2500
      },
  });

  if ($("#message").text() != "")
    $("#message").slideDown()

  setTimeout(function(){
    $("#message").slideUp()
  }, 4500);




  $('.hover').hover(function(){
            // Hover over code
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip"></p>')
            .text(title)
            .appendTo('body')
            .fadeIn('slow');
    }, function() {
            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
    }).mousemove(function(e) {
            var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates
            $('.tooltip')
            .css({ top: mousey, left: mousex })
    });


});