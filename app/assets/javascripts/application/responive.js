;(function(){
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// OPTIONS
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	var eski_options = {
		mode: "production", // development | production
		mobile_active_width: 768,
		less_watch: false
	};

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// CHECK DEVELOPMENT
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	if( document.cookie.match(/mobidev/gi) || location.hash.match(/mobidev|eskimobi|mobitest/gi) ){
		var date = new Date(new Date().getTime() + 60 * 60 * 1000);
		document.cookie = "mobidev=1; path=/; expires=" + date.toUTCString();
		eski_options.mode = 'mobidev';
	}

	if( location.hash.match(/mobioff/gi) ){
		var date = new Date(0);
		document.cookie = "mobidev=; path=/; expires=" + date.toUTCString();
	}

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// INIT
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	function eski_init(){
		if(
			screen.width < eski_options.mobile_active_width &&
			eski_options.mode.match(/production|mobidev/gi) &&
			/firefox|fennec/i.test(navigator.userAgent) == false
		){
			var body_check = function(){
		    setTimeout(function(){
	        var b = document.querySelector('body');
	        if( b ){
            check_return_panel();
            if( document.cookie.indexOf('mobi-off') == -1 && document.cookie.indexOf('mobi-panel') == -1 ){
              b.classList.add('eskimobi_responsive');
              eski_functions();
            }
	        }else{
	          body_check();
	        }
		    }, 100);
			};
			body_check();
		}
	}

	function check_return_panel(){
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // RETURN TO DESKTOP
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    window.eski_desktop = function () {
      document.cookie = 'mobi-panel=;path=/;expires=' + (new Date(+(new Date) + (1000 * 60 * 60 * 24)).toGMTString());
      location.reload();
    };
    if (document.cookie.indexOf('mobi-panel') > -1) {
      var date = new Date(0);
      window.panel_yes = function () {
        document.cookie = "mobi-panel=; path=/; expires=" + date.toUTCString();
        document.cookie = "mobi-off=; path=/; expires=" + date.toUTCString();
        location.reload();
      };
      window.panel_no = function () {
        document.cookie = "mobi-panel=; path=/; expires=" + date.toUTCString();
        document.cookie = 'mobi-off=;path=/;expires=' + (new Date(+(new Date) + (1000 * 60 * 60 * 24 * 7)).toGMTString());
        var el = document.getElementById('eski_mobi');
        el.parentNode.removeChild(el);
      };
      // var return_to_mobile = '<div><style>#eski_mobi,#eski_mobi #eski-right #eski-group #eski-btn a{overflow:hidden;-webkit-box-sizing:border-box;font-weight:700}#eski_mobi{position:fixed;width:100%;z-index:99999999;top:0;left:0;margin:0;padding:0;background:#3fd7cb;color:#0d3d39;font-family:Helvetica,sans-serif;font-size:16px;letter-spacing:-.5px;border-bottom:1px solid #0d3d39;box-shadow:0 2px 20px rgba(0,0,0,.5)}#eski_mobi .inside{margin:.5em 0}#eski_mobi #eski-left{float:left;width:70%}#eski_mobi #eski-right{float:left;width:30%}#eski_mobi .clear{clear:both}#eski_mobi #eski-left p{margin:0 .5em;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;text-align:center;line-height:2em;text-shadow:0 1px 0 #bef1ed}#eski_mobi #eski-right #eski-group{width:100%;padding:0}#eski_mobi #eski-right #eski-group #eski-btn{padding:5px 0;width:45%;margin-right:5%;float:left}#eski_mobi #eski-right #eski-group #eski-btn a{width:100%;min-width:30px;padding:.5em 0;display:block;color:#fff;text-decoration:none;text-align:center;border-radius:.5em;text-shadow:0 -1px 0 #0d3d39;background:#1a7d75;box-shadow:0 1px 0 #041312,0 3px 4px #1e9289;-webkit-transition:all 100ms ease-in-out;-moz-transition:all 100ms ease-in-out;-ms-transition:all 100ms ease-in-out;-o-transition:all 100ms ease-in-out;transition:all 100ms ease-in-out}#eski_mobi #eski-right #eski-group #eski-btn a:hover{color:#166761;background:#fff;text-shadow:none;box-shadow:0 0 20px #fff,0 1px 0 #041312,0 3px 4px #1e9289}</style>           <div id="eski_mobi"> <div id="eski-inside"> <div id="eski-left"> <p>\u041E\u0442\u043A\u0440\u044B\u0442\u044C \u043C\u043E\u0431\u0438\u043B\u044C\u043D\u0443\u044E \u0432\u0435\u0440\u0441\u0438\u044E \u0441\u0430\u0439\u0442\u0430?</p> </div> <div id="eski-right"> <div id="eski-group"> <div id="eski-btn"> <a href="javascript:panel_yes()">\u0414\u0430</a> </div> <div id="eski-btn"> <a href="javascript:panel_no()">\u041D\u0435\u0442</a> </div> <div id="eski-clear"></div> </div> </div> <div id="eski-clear"></div> </div></div></div>';
      // document.querySelector('body').insertAdjacentHTML('afterBegin', return_to_mobile);
    }
	}

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// ESKI SCRIPTS
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	function eski_functions (){
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// LESS WATCH
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		if( eski_options.less_watch ){
			var less_timer = function(){
				setTimeout(function(){
					if( typeof less == 'object' ){
						less.watch();
					}else{
						less_timer();
					}
				}, 500);
			}
			less_timer();
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// FUNCTIONS
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		var insert = function(el, position, html){
			if( position == 'after' ){ position = 'afterend' }
			if( position == 'before' ){ position = 'beforeBegin' }
			if( el && html ){
				el.insertAdjacentHTML(position, html);
			}
		}
		var add_el = function(name, attr, inner, el){
			var cr = document.createElement(name);
			cr.innerHTML = inner;
			if(attr){
				for( key in attr ){
					if( attr.hasOwnProperty( key ) ){
						cr.setAttribute( key, attr[key])
					}
				}
			}
			if( el && cr ){
				el.appendChild( cr );
			}
		}
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// VARS
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		var head = document.querySelector('head');
		var body = document.querySelector('body');
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// ADD META
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		add_el( 'meta', { name: 'HandheldFriendly', content: "True" }, '', head);
		add_el( 'meta', { name: 'format-detection', content: "telephone=yes" }, '', head);
		add_el( 'meta', { name: 'apple-mobile-web-app-title', content: "Setano" }, '', head);
		add_el( 'meta', { name: 'MobileOptimized', content: "320" }, '', head);
		add_el( 'meta', { name: 'viewport', content: "width=device-width, initial-scale=1, user-scalable=no" }, '', head);
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// ACTIVE MOBILE STYLE
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		body.classList.add('eskimobi_responsive');
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// CHECK MOBILE WIDTH
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		var slider_timer;
		function resize(){
			clearTimeout(slider_timer);
			slider_timer = setTimeout(function () {
				if( screen.width > eski_options.mobile_active_width ){
					location.href = location.href;
				}
			});
		}
		resize();
		window.onresize = resize;
		// //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// // ADD AUTHOR
		// //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// var utm_site = location.hostname.replace(/www\./gi, '').replace(/\./gi, '_');
		// if( !utm_site ){ utm_site = ''; }
		// add_el( 'div', {id: 'author'},
		// 	"<a href='https://eski.mobi/services/lite/?utm_source="+utm_site+"&utm_medium=copyright&utm_campaign=footer'>"+
		// 		"<span class='txt'>&#x041C;&#x043E;&#x0431;&#x0438;&#x043B;&#x044C;&#x043D;&#x044B;&#x0439; &#x0441;&#x0430;&#x0439;&#x0442; &#x0441;&#x0434;&#x0435;&#x043B;&#x0430;&#x043D; &#x0432; <span></span></span>"+
		// 	"</a>",
		// 	body.querySelector('.footer')
		// );
		// //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// // ADD RETURN TO DESKTOP
		// //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// add_el('div', {id: 'returntomobile'},
    //   '<a class="desktop" href="javascript:void(0)" onclick="if(window.confirm(\'\u0412\u043D\u0438\u043C\u0430\u043D\u0438\u0435! \u0412\u044B \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u044C\u043D\u043E \u0445\u043E\u0442\u0438\u0442\u0435 \u043F\u043E\u043A\u0438\u043D\u0443\u0442\u044C \u043C\u043E\u0431\u0438\u043B\u044C\u043D\u044B\u0439 \u0441\u0430\u0439\u0442 \u0438 \u043F\u0435\u0440\u0435\u0439\u0442\u0438 \u043A \u043F\u043E\u043B\u043D\u043E\u0439 \u0432\u0435\u0440\u0441\u0438\u0438?\')){eski_desktop();}return false;">\u041F\u043E\u043B\u043D\u0430\u044F \u0432\u0435\u0440\u0441\u0438\u044F \u0441\u0430\u0439\u0442\u0430</a>',
    //   body.querySelector('.footer')
	  // );


//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// SCRIPTs
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		// ADD MENU
		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		var init_menu = function(){
			insert(
				body.querySelector('.nav-box'),
				'before',
				'<div class="menu_outer"><a class="btn_stylish" id="mobile_menu" href="javascript:void(0)">\u041C\u0435\u043D\u044E</a></div>'
			);
			function toggle(e){
				body.querySelector('.menu_outer').classList.toggle('js_open');
			}
			var m = body.querySelector('#mobile_menu');
			if( m ){
				m.addEventListener("click", toggle);
			}

			function toggle2(e){
				body.querySelector('.menu_outer_2').classList.toggle('js_open');
			}
			var fl = body.querySelector('.filter');
			if (fl){
				insert(
					body.querySelector('.filter'),
					'before',
					'<div class="menu_outer_2"><a class="btn_stylish" id="search_menu" href="javascript:void(0)">\u041F\u043E\u0438\u0441\u043A</a></div>'
				);
				body.querySelector('#search_menu').addEventListener("click", toggle2);
			};

			var sl = body.querySelector('.product_slider');
			if (typeof(sl) != 'undefined' && sl != null){
				insert(
					sl,
					'before',
					'<div class="true-slider">'+sl.outerHTML+'</div>'
				);
				sl.remove();
			};

		};
		init_menu();


//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// END SCRIPTs
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


	} // end if

	eski_init();

})();
