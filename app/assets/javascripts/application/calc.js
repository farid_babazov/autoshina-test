$(document).ready(function(){  
  var old_width = $("#old_width").val()
  var old_height = $("#old_height").val()
  var old_diameter = $("#old_diameter").val()
  var new_width = $("#new_width").val()
  var new_height = $("#new_height").val()
  var new_diameter = $("#new_diameter").val()
  var clearance = (((25.4*new_diameter) / 2)+((new_width*new_height) / 100))-(((25.4*old_diameter) / 2)+((old_width*old_height) / 100))
  
  $(".tire_size form select").change(function() {
    old_width = $("#old_width").val()
    old_height = $("#old_height").val()
    old_diameter = $("#old_diameter").val()
    new_width = $("#new_width").val()
    new_height = $("#new_height").val()
    new_diameter = $("#new_diameter").val()
    clearance = (((25.4*new_diameter) / 2)+((new_width*new_height) / 100))-(((25.4*old_diameter) / 2)+((old_width*old_height) / 100))
    set_table_values()

  });

  $(".rim_size form select").change(function() {
    width = $("#tire_width").val()
    profile = $("#tire_height").val()
    radius = $("#tire_diameter").val()
    
    if(profile<50) scalar=0.85;
    if(profile>=50) scalar=0.7;

    rimWidthMin = (Math.round(((width*scalar)*0.03937)*2))/2;
    rimWidthMax = (rimWidthMin+1.5);
    $("#diameter_rim").text($("#tire_diameter").val())
    $("#min_width_rim").text(rimWidthMin)
    $("#max_width_rim").text(rimWidthMax)

  });

  function set_table_values(){
    $('.compare_table .width_td td:first').next().text(old_width)
    $('.compare_table .width_td td:last').prev().text(new_width)
    $('.compare_table .width_td td:last').text(new_width-old_width)

    $('.compare_table .height_td td:first').next().text( Math.round( ( (old_width*old_height) / 100 ) ) )
    $('.compare_table .height_td td:last').prev().text( Math.round( ( (new_width*new_height) / 100 ) ) )
    $('.compare_table .height_td td:last').text(Math.round( ( (new_width*new_height) / 100 ) ) - Math.round( ( (old_width*old_height) / 100 ) ) )

    $('.compare_table .inside_diameter_td td:first').next().text(Math.round(25.4*old_diameter)  )
    $('.compare_table .inside_diameter_td td:last').prev().text(Math.round(25.4*new_diameter)  )
    $('.compare_table .inside_diameter_td td:last').text(Math.round(25.4*new_diameter) - Math.round(25.4*old_diameter))

    $('.compare_table .outside_diameter_td td:first').next().text(Math.round(25.4*old_diameter+2*( (old_width*old_height) / 100 ))  )
    $('.compare_table .outside_diameter_td td:last').prev().text(Math.round(25.4*new_diameter+2*( (new_width*new_height) / 100 ))  )
    $('.compare_table .outside_diameter_td td:last').text(Math.round(25.4*new_diameter+2*( (new_width*new_height) / 100 )) - Math.round(25.4*old_diameter+2*( (old_width*old_height) / 100 ))  )
    
    $('.compare_table .clearance td:last').text( clearance.toFixed(1) )


    // $( ".real_speed td:last" ).prev().text($( ".jq_slider" ).slider( "value" ) + " км/ч");
    speed = ($( ".jq_slider" ).slider( "value" ));
    new_speed = Math.round((Math.round(new_width*new_height*0.02+new_diameter*25.4)  /  Math.round(old_width*old_height*0.02+old_diameter*25.4))*speed*100)/100
    $(".real_speed td:last").prev().text(new_speed + "км/ч");
    $(".difference_speed td:last").prev().text(((new_speed - speed).toFixed(2)) + " км/ч");

  }

  $('.jq_slider').slider({
    value:60,
    min: 10,
    max: 200,
    step: 1,
    slide: function( event, ui ) {
      old_width = $("#old_width").val()
      old_height = $("#old_height").val()
      old_diameter = $("#old_diameter").val()
      new_width = $("#new_width").val()
      new_height = $("#new_height").val()
      new_diameter = $("#new_diameter").val()
      var new_speed = Math.round((Math.round(new_width*new_height*0.02+new_diameter*25.4)  /  Math.round(old_width*old_height*0.02+old_diameter*25.4))*ui.value*100)/100
      $(".jq_slider a span").text(ui.value + " км/ч");
      
      $(".real_speed td:last").prev().text(new_speed + " км/ч");
      $(".difference_speed td:last").prev().text(((new_speed - ui.value).toFixed(2)) + " км/ч");
      $( "#speed" ).val( ui.value + " км/ч");
    },
    create: function( event, ui ) {
      $(".jq_slider a").append("<span></span>")
      $( ".jq_slider a span" ).text($( ".jq_slider" ).slider( "value" ) + " км/ч");
      $( "#speed" ).val(ui.value + " км/ч");
      $( ".real_speed td:last" ).prev().text($( ".jq_slider" ).slider( "value" ) + " км/ч");
      
    }
  });

  set_table_values()


  $(".inch_size select").change(function() {
    width = $("#tire_width_i").val()
    height = $("#tire_height_i").val()
    radius = $("#tire_diameter_i").val()
    
    // if(profile<50) scalar=0.85;
    // if(profile>=50) scalar=0.7;

    // rimWidthMin = (Math.round(((width*scalar)*0.03937)*2))/2;
    // rimWidthMax = (rimWidthMin+1.5);
    // $("#diameter_rim").text($("#tire_diameter").val())
    // $("#min_width_rim").text(rimWidthMin)
    // $("#max_width_rim").text(rimWidthMax)

    $(".inch_size_table #height_inch").text(height)
    $(".inch_size_table #width_inch").text(width)
    $(".inch_size_table #diameter_inch").text(radius)
    width_tire = Math.round(width*25.4)
    height_tire = Math.round((((height - radius)*25.4)*100) / ((width*25.4)*2))
    $(".inch_size_table #width_m").text(width_tire)
    $(".inch_size_table #height_m").text( height_tire )
    $(".inch_size_table #diameter_m").text(radius)
    if (width_tire%10 < 5)
      h = Math.round(width_tire/10)*10+5
    else
      h = Math.round(width_tire/10)*10-5

    if ((height_tire%10 >= 0 && height_tire%10 <= 3) || (height_tire%10 > 7))
      w = Math.round(height_tire/10)*10
    else if (height_tire%10 ==4)
      w = Math.round(height_tire/10)*10+5
    else if (height_tire%10 >=5 && height_tire%10 <=7)
      w = Math.round(height_tire/10)*10-5
    $(".inch_size_table #width_o").text(h)
    $(".inch_size_table #height_o").text(w)
    $(".inch_size_table #diameter_o").text(radius)



  });
});
