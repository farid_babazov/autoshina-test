$(document).ready(function() {
  
  // update_vendors();
  disabled_oil_fields()
  disabled_rim_fields()
  disabled_battery_fields()
  enabled_tire_fields()
  // fetch_fields();
    $('#tire_category').change(function() {
      // update_vendors();
      if ($('#tire_category').val()==""){
        $(".tire_fields").empty();
        $(".tire_checkbox").empty();
      }
      else{
        fetch_fields();
      }
    });

    $('#oil_category').change(function() {
      // update_vendors();
      if ($('#oil_category').val()==""){
        $(".oil_fields").empty();
        $(".oil_checkbox").empty();
      }
      else{
        fetch_fields();
      }
    });
    
    if ($('#search_type_part_id_2').prop("checked")){ 
      $(".search_fields").css("marginLeft", "-240px")
        disabled_tire_fields()
        disabled_rim_fields()
        disabled_battery_fields()
        enabled_oil_fields()
    }
    if ($('#search_type_part_id_1').prop("checked")){ 
      $(".search_fields").css("marginLeft", "0px")
        disabled_oil_fields()
        disabled_rim_fields()
        disabled_battery_fields()
        enabled_tire_fields()
    }
    if ($('#search_type_part_id_3').prop("checked")){ 
      $(".search_fields").css("marginLeft", "-480px")
        disabled_oil_fields()
        disabled_tire_fields()
        disabled_rim_fields()
        enabled_battery_fields()
    }
    if ($('#search_type_part_id_4').prop("checked")){ 
      $(".search_fields").css("marginLeft", "-720px")
        disabled_oil_fields()
        disabled_tire_fields()
        disabled_battery_fields()
        enabled_rim_fields()
    }
    
    $("#search_type_part_id_1, #search_type_part_id_2, #search_type_part_id_3, #search_type_part_id_4").click(function(e){
      $('.search_fields > *').removeClass('active');
      // var elem = $(e.target),
      //     connected_elem = $(elem).data('connected');
      // $("."+connected_elem).addClass('active');
      
      if  ($('#search_type_part_id_1').prop("checked")){
        $(".tire").addClass('active');
        $("label[for='search_type_part_id_1']").addClass("check");
        $("label[for='search_type_part_id_2']").removeClass("check")
        $("label[for='search_type_part_id_3']").removeClass("check")
        $("label[for='search_type_part_id_4']").removeClass("check")
        $(".search_fields").animate({marginLeft: "0"}, 100);
        disabled_oil_fields()
        disabled_rim_fields()
        disabled_battery_fields()
        enabled_tire_fields()
        $("#oil_category :first").attr("selected", "selected");
      }
      else if ($('#search_type_part_id_2').prop("checked")){
        $(".oil").addClass('active');
        $("label[for='search_type_part_id_2']").addClass("check")
        $("label[for='search_type_part_id_1']").removeClass("check")
        $("label[for='search_type_part_id_3']").removeClass("check")
        $("label[for='search_type_part_id_4']").removeClass("check")
        $(".search_fields").animate({marginLeft: "-240"}, 100);
        disabled_tire_fields()
        disabled_rim_fields()
        disabled_battery_fields()
        enabled_oil_fields()
        $("#tire_category :first").attr("selected", "selected");
      }
      else if ($('#search_type_part_id_3').prop("checked")){
        $(".battery").addClass('active');
        $("label[for='search_type_part_id_3']").addClass("check")
        $("label[for='search_type_part_id_1']").removeClass("check")
        $("label[for='search_type_part_id_2']").removeClass("check")
        $("label[for='search_type_part_id_4']").removeClass("check")
        $(".search_fields").animate({marginLeft: "-480"}, 100);
        disabled_oil_fields()
        disabled_tire_fields()
        disabled_rim_fields()
        enabled_battery_fields()
        $("#tire_category :first").attr("selected", "selected");
      }
      else if ($('#search_type_part_id_4').prop("checked")){
        $(".rim").addClass("active");
        $("label[for='search_type_part_id_4']").addClass("check");
        $("label[for='search_type_part_id_1']").removeClass("check")
        $("label[for='search_type_part_id_2']").removeClass("check")
        $("label[for='search_type_part_id_3']").removeClass("check")
        $(".search_fields").animate({marginLeft: "-720"}, 100);
        disabled_oil_fields()
        disabled_tire_fields()
        disabled_battery_fields()
        enabled_rim_fields()
        $("#tire_category :first").attr("selected", "selected");
      }
    })
    function enabled_oil_fields(){
      $('#oil_category').prop('disabled', false);
      $(".oil_fields select").each(function( index ) {
        $( this ).prop('disabled', false);
      });
      $('.oil input[type="submit"]').prop('disabled', false);
    }
    function enabled_tire_fields(){
      $('#tire_category').prop('disabled', false);
      $(".tire_fields select").each(function( index ) {
        $( this ).prop('disabled', false);
      });
      $('.tire input[type="submit"]').prop('disabled', false);
    }
    function enabled_battery_fields(){
      $('#battery_category').prop('disabled', false);
      $(".battery_fields select").each(function( index ) {
        $( this ).prop('disabled', false);
      });
      $('.battery input[type="submit"]').prop('disabled', false);
    }
    function enabled_rim_fields(){
      $('#rim_category').prop('disabled', false);
      $(".rim_fields select").each(function( index ) {
        $( this ).prop('disabled', false);
      });
      $('.rim input[type="submit"]').prop('disabled', false);
    }
    function disabled_tire_fields(){
      // $('#tire_category').attr("disabled","disabled");
      // $(".tire_fields select").each(function( index ) {
      //   $( this ).attr("disabled","disabled");
      // });
      $('#oil_category').prop('disabled', false);
      $(".oil_fields select").each(function( index ) {
        $( this ).prop('disabled', false);
      });
      $('.oil input[type="submit"]').prop('disabled', false);
      $('.tire input[type="submit"]').attr('disabled', 'disabled');
      // $('#oil_vendor').prop('disabled', false);
      $('#oil_category').attr("disabled","disabled");
    }
    function disabled_tire_fields(){
      $('#tire_category').attr("disabled","disabled");
      $(".tire_fields select").each(function( index ) {
        $( this ).attr("disabled","disabled");
      });
      $('.tire input[type="submit"]').attr('disabled', 'disabled');
    }
    function disabled_oil_fields(){
      $('#oil_category').attr("disabled","disabled");
      $(".oil_fields select").each(function( index ) {
        $( this ).attr("disabled","disabled");
      });
      $('.oil input[type="submit"]').attr('disabled', 'disabled');
    }
    function disabled_battery_fields(){
      $('#battery_category').attr("disabled","disabled");
      $(".battery_fields select").each(function( index ) {
        $( this ).attr("disabled","disabled");
      });
      $('.battery input[type="submit"]').attr('disabled', 'disabled');
    }
    function disabled_rim_fields(){
      $('#rim_category').attr("disabled","disabled");
      $(".rim_fields select").each(function( index ) {
        $( this ).attr("disabled","disabled");
      });
      $('.rim input[type="submit"]').attr('disabled', 'disabled');
    }


    function fetch_fields(){
      if  ($('#search_type_part_id_1').prop("checked")){
        var category = $('#tire_category').val() 
      }else{
        var category = $('#oil_category').val()
      }
      if ($('#search_type_part_id_1').prop("checked")){
        var type = 1;
      }else if ($('#search_type_part_id_2').prop("checked")){
        var type = 2;
      }
      $.ajax({
        method: 'GET',
        dataType: "script",
        url: "/products/fetch_fields",
        data: ({category_id : category, type: type})
      });
    }

    $('.models input[type="button"]').attr("disabled","disabled");


    $("#search_of_car_type").click(function(){
      $('.fields > *').removeClass('active');
      $('.models').addClass('active');
      $(".fields").animate({marginLeft: "-240"}, 100);
      $('.tire input[type="submit"]').attr('disabled', 'disabled');
      $('#tire_category').attr("disabled","disabled");
      $(".tire_fields select").each(function( index ) {
        $( this ).attr("disabled","disabled");
      });
      $(".models select").each(function( index ) {
        $( this ).prop('disabled', false);
      });
      if ($('#car_model').val() == ""){
        $('#car_model').attr("disabled","disabled");
      }
      if ($('#car_year').val() == ""){
        $('#car_year').attr("disabled","disabled");
      }
      if ($('#car_modification').val() == ""){
        $('#car_modification').attr("disabled","disabled");
      }
      $('.models input[type="button"]').attr("disabled","disabled");
    })

    $("#search_of_tire_type").click(function(){
      $('.fields > *').removeClass('active');
      $('.tire_type').addClass('active');
      $(".fields").animate({marginLeft: "0"}, 100);
      $('.tire input[type="submit"]').prop('disabled', false);
      $('#tire_category').prop('disabled', false);
      $(".tire_fields select").each(function( index ) {
        $( this ).prop('disabled', false);
      });
      $(".models select").each(function( index ) {
        $( this ).attr("disabled","disabled");
      });
      $('.models input[type="button"]').attr("disabled","disabled");
    })

    $('#car_manufacturer').change(function() {
      manufacturer = $('#car_manufacturer').val()
      $.ajax({
        method: 'GET',
        dataType: "script",
        url: "/car_models/fetch_models",
        data: ({manufacturer : manufacturer})
      });

      $('#car_year').attr("disabled","disabled");
      $('#car_modification').attr("disabled","disabled");
      $('.models input[type="button"]').attr("disabled","disabled");
    });

    $('#car_model').change(function() {
      model = $('#car_model').val()
      $.ajax({
        method: 'GET',
        dataType: "script",
        url: "/car_models/fetch_years",
        data: ({model : model})
      });

      $('#car_modification').attr("disabled","disabled");
      $('.models input[type="button"]').attr("disabled","disabled");
    });

    $('#car_year').change(function() {
      year = $('#car_year').val()
      model = $('#car_model').val()
      $('#car_modification').prop('disabled', false);
      $.ajax({
        method: 'GET',
        dataType: "script",
        url: "/car_models/fetch_modifications",
        data: ({model : model, year: year})
      });
      $('.models input[type="button"]').attr("disabled","disabled");
    });
    $('#car_modification').change(function() {
      modification = $('#car_modification').val()
      $('.models input[type="button"]').prop('disabled', false);
    });
    $('.models input[type="button"]').click(function() {
      modification = $('#car_modification').val()
      $.ajax({
        method: 'GET',
        dataType: "script",
        url: "/car_models/fetch_sizes",
        data: ({modification: modification})
      });
    });


});