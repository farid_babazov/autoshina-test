$(document).ready(function() {
  $(":input").inputmask();

  $('.js-select-deleviry').on('change', function() {
    var $delivery_cost = $(this).find(':selected').data('delivery-cost');
    var $delivery_warning_message = $('.user-order__delivery-warning');
    var $js_order_price = $('.js-order-price span');
    var $js_delivery_cost = $('.js-delivery-cost span');
    var $js_total_order_cost = $('.js-total-order-cost span');
    var total_order_cost;
    var order_price = parseFloat($js_order_price.html());
    var delivery_cost = parseFloat($delivery_cost);

    if (delivery_cost == 0) {
      $delivery_warning_message.show();
    } else {
      $delivery_warning_message.hide();
    }

    if (isNaN(delivery_cost)) {
      delivery_cost = 0
    }

    total_order_cost = order_price + delivery_cost;

    $js_delivery_cost.html(delivery_cost.toFixed(2));
    $js_total_order_cost.html(total_order_cost.toFixed(2));
  });

  $("input[type=radio][name='order[payment_type]']").on('change', function() {
    var checked_val = $(this).val();
    var $js_submit_order = $('.js-submit-order');

    if (checked_val == 'card') {
      $js_submit_order.val('Перейти к оплате');
    } else {
      $js_submit_order.val('Заказать');
    }
  });

  $('#user_order_form input, #user_order_form textarea, #user_order_form select').on('change paste keyup', function() {
    var current_element = $(this).attr('name');
    var field_name = current_element.match("\\[(.*)]");
    var $js_form_errors__field = '.js-form-errors__' + field_name[1];
    var $error_div = $('#user_order_form').find($js_form_errors__field);

    if ($(this).val() ) {
      $error_div.hide();
    } else {
      $error_div.show();
    }
  });
});
