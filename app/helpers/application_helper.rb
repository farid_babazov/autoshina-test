module ApplicationHelper
  def tires_checked?
    params[:search_type].present? and params[:search_type][:part_id].present? and params[:search_type][:part_id] == '1' or params[:search_type].nil?
  end

  def oils_checked?
    params[:search_type].present? and params[:search_type][:part_id].present? and params[:search_type][:part_id] == '2'
  end

  def accumulator_checked?
    params[:search_type].present? and params[:search_type][:part_id].present? and params[:search_type][:part_id] == '3'
  end

  def rim_checked?
    params[:search_type].present? and params[:search_type][:part_id].present? and params[:search_type][:part_id] == '4'
  end

  def category_checked
    if params[:search_type].present? and params[:search_type][:category_id].present?
      params[:search_type][:category_id]
    end
  end

  def vendor_checked
    if params[:search_type].present? and params[:search_type][:vendor_id].present?
      params[:search_type][:vendor_id]
    else
      ""
    end
  end

  def tire_vendors
    if params[:search_type].present? and params[:search_type][:part_id] == "1" and params[:search_type][:category_id].present?
      (Category.find(params[:search_type][:category_id]).vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    else
      (Category.where(part_id: 1).first.vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    end
  end

  def oil_vendors
    if params[:search_type].present? and params[:search_type][:part_id] == "2" and params[:search_type][:category_id].present?
      (Category.find(params[:search_type][:category_id]).vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    else
      (Category.where(part_id: 2).first.vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    end
  end

  def battery_vendors
    if params[:search_type].present? and params[:search_type][:part_id] == "3" and params[:search_type][:category_id].present?
      (Category.find(params[:search_type][:category_id]).vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    else
      (Category.where(part_id: 3).first.vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    end
  end
  def rim_vendors
    if params[:search_type].present? and params[:search_type][:part_id] == "4" and params[:search_type][:category_id].present?
      (Category.find(params[:search_type][:category_id]).vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    else
      (Category.where(part_id: 4).first.vendors.map { |c| [c.name, c.id] }).insert(0, ["Производитель", ""])
    end
  end

  def category_fields_select id
    # if params[:search_size].present? and params[:search_type][:vendor_id].present?
      params[:search_size].fetch("field_#{id}", "") if params[:search_size].present?
    # end
  end

  def category_fields_checked value
    if params[:search_season].present?
      params[:search_season].keys.include? value
    end
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge({:sort => column, :direction => direction}), {:class => css_class}
  end

  def swf_tag(path)
    "<embed src=\'path/to/resource\' />".html_safe
  end

  def payment_method
    if params[:order].present? and params[:order][:payment_type] == 'card'
      'card'
    else
      'cash'
    end
  end

  def delivery_cost
    if params[:order].present? and params[:order][:delivery_id].present?
      Delivery.find(params[:order][:delivery_id]).cost
    end
  end

  def delivery_region_id
    params[:order][:delivery_id] if params[:order].present?
  end
end
