module ProductsHelper

  def currency_checked product
    if cookies[:currency] == "dollar"
      product.currency_price("dollar")
    else
      product.currency_price("som")
    end
  end

  def discount product
    if cookies[:currency] == "dollar"
      p = product.conversion_of_currency.to_f
      dst = p - ((p/100) * product.discount)
      number_to_currency dst, format: '%n%u', unit: "$"
    else
      number_to_currency product.discount_price, format: '%n %u', unit: "сом"
    end
  end

  def price_difference product
    if cookies[:currency] == "dollar"
      p = product.conversion_of_currency.to_f
      dst = p - ((p/100) * product.discount)
      number_to_currency (p-dst), format: '%n%u', unit: "$"
    else
      number_to_currency (product.cost - product.discount_price), format: '%n %u', unit: "сом"
    end
  end

  def order_price_difference order
    if cookies[:currency] == "dollar"
      p = order.currency_total_price
      dst = p - ((p/100) * product.discount)
      number_to_currency (p-dst), format: '%n%u', unit: "$"
    else
      number_to_currency (order.total_price - order.discount_total_price_s), format: '%n %u', unit: "сом"
    end
  end

  def total_price order
    if cookies[:currency] == "dollar"
     number_to_currency order.discount_total_price_d, format: '%n%u', unit: "$"
    else
     number_to_currency order.discount_total_price_s, format: '%n %u', unit: "сом"
    end
  end

  def saving order
    if cookies[:currency] == "dollar"
      number_to_currency order.currency_total_price - order.discount_total_price_d, format: '%n%u', unit: "$"
    else
      number_to_currency (order.total_price - order.discount_total_price_s), format: '%n %u', unit: "сом"
    end
  end

  def type_helper part
    case part.name
    when "Шины"
      "шины"
    when "Автомасла"
      "масла"
    when "Аккумуляторы"
      "аккумулятора"
    when "Диски"
      "диска"
    else
      ""
    end
  end

end
