class RestartFeaturedProductsWorker
  # include Sidekiq::Worker

  def perform
    # c = Product.featured.count - 25
    Product.featured.each_with_index do |p, index|
      if index < 25
        p.update_attributes featured: 1
      else
        p.update_attributes featured: 0
      end
    # Product.featured.first(25).each do |p|
    end
  end
end
