class OrderMailer < ActionMailer::Base
  default from: "sales@autoshina.kg"

  def new_order(order)
    @order = order
    mail(to: "info@autoshina.kg", subject: 'Сообщение о новом заказе')
  end

  def customer_order(order)
    @order = order
    mail(to: @order.email, subject: 'Сообщение о Вашем заказе')
  end
end
