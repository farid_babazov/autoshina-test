# == Schema Information
#
# Table name: banners
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  link       :string(255)
#  file       :string(255)
#  active     :boolean
#  site       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  order      :integer
#

class Banner < ActiveRecord::Base

  mount_uploader :file, FileUploader
  validates_presence_of :file
  scope :slides, -> { where("site = 'slider' AND active = TRUE").order("banners.order asc") }
  scope :active, -> { where active: true }
  
  validates :file, :presence => true

end
