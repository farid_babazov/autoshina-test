# == Schema Information
#
# Table name: car_modifications
#
#  id                  :integer          not null, primary key
#  car_model_id        :integer
#  year                :string(255)
#  modification        :string(255)
#  default             :string(255)
#  replacement         :string(255)
#  tuning              :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  car_manufacturer_id :integer
#

class CarModification < ActiveRecord::Base
  belongs_to :car_model
  belongs_to :car_manufacturer
  validates_presence_of :car_model_id, :car_manufacturer_id#, :year, :modification
end
