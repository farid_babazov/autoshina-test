# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  image      :string(255)
#  order      :string(255)
#  created_at :datetime
#  updated_at :datetime
#  remote_id  :integer
#  part_id    :integer
#

class Category < ActiveRecord::Base
  has_many :products
  has_many :vendors
  has_many :categories_fields
  has_many :categories_fields_associations
  has_many :categories_fields_values, through: :categories_fields
  mount_uploader :image, CategoriesUploader
  belongs_to :part

  default_scope -> { order(:order) }
end
