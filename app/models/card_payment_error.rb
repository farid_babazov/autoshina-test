# == Schema Information
#
# Table name: card_payment_errors
#
#  id          :integer          not null, primary key
#  code        :string(255)
#  title       :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class CardPaymentError < ActiveRecord::Base
  scope :code, -> (code) { where(code: code) }
end
