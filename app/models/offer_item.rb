# == Schema Information
#
# Table name: offer_items
#
#  id               :integer          not null, primary key
#  title            :string
#  snippet          :string
#  text             :text
#  meta_keywords    :string
#  meta_description :string
#  image            :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class OfferItem < ActiveRecord::Base
  mount_uploader :image, OfferUploader
  paginates_per 15
  default_scope -> { order("created_at desc") }
end
