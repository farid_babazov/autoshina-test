# == Schema Information
#
# Table name: car_models
#
#  id                  :integer          not null, primary key
#  car_manufacturer_id :integer
#  model               :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#

class CarModel < ActiveRecord::Base
  belongs_to :car_manufacturer
  has_many :car_modifications
  validates :model, uniqueness: true
  validates_presence_of :model, :car_manufacturer_id
end
