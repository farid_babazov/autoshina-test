# == Schema Information
#
# Table name: car_manufacturers
#
#  id         :integer          not null, primary key
#  remote_id  :integer
#  title      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class CarManufacturer < ActiveRecord::Base

  has_many :car_models
  has_many :car_modifications

  validates :title, uniqueness: true
  validates_presence_of :title
end
