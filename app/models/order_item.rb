# == Schema Information
#
# Table name: order_items
#
#  id                    :integer          not null, primary key
#  order_id              :integer
#  product_id            :integer
#  quantity              :integer
#  created_at            :datetime
#  updated_at            :datetime
#  product_name          :string(255)
#  product_internal_name :string(255)
#  product_cost          :float
#  product_discount      :float            default(0.0)
#

class OrderItem < ActiveRecord::Base
  belongs_to :order, touch: true
  belongs_to :product

  before_validation :update_product_attributes
  validates_presence_of :product_id, :order_id
  validates :quantity, numericality: { greater_than: 0 }

  def total_price
    quantity * product_cost
  end

  def total_discounted_price
    discount_cost = self.product_cost - ((self.product_cost/100) * self.product_discount)
    self.quantity * discount_cost
  end

  # only 8 tires could be delivered at one order.
  def is_tire?
    self.product.part_id == 1
  end

  private
    def update_product_attributes
      self.product_name =   self.product.try :name
      self.product_internal_name =  self.product.try :internal_name
      self.product_cost =  self.product.try :cost
      self.product_discount = self.product.try :discount
    end
end
