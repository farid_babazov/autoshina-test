# == Schema Information
#
# Table name: cars
#
#  id           :integer          not null, primary key
#  remote_id    :integer
#  vendor       :string(255)
#  car          :string(255)
#  year         :datetime
#  modification :string(255)
#  default      :string(255)
#  replacement  :string(255)
#  tuning       :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class Car < ActiveRecord::Base
end
