# == Schema Information
#
# Table name: news_items
#
#  id               :integer          not null, primary key
#  title            :string(255)
#  snippet          :string(255)
#  text             :text
#  meta_keywords    :string(255)
#  meta_description :string(255)
#  image            :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class NewsItem < ActiveRecord::Base
  
  mount_uploader :image, NewsUploader
  paginates_per 15
  default_scope -> { order("created_at desc") }
end
