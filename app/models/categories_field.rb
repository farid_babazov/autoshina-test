# == Schema Information
#
# Table name: categories_fields
#
#  id             :integer          not null, primary key
#  remote_id      :integer
#  category_id    :integer
#  field_name     :string(255)
#  association_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#  show_in_browse :integer
#  show_in_search :integer
#  type_field     :string(255)
#  search_type    :string(255)
#

class CategoriesField < ActiveRecord::Base
  belongs_to :category
  has_many :product_fields_values, dependent: :destroy
  has_many :categories_fields_values

  validates_presence_of :category_id, :field_name, :show_in_browse, :type_field, :search_type
end
