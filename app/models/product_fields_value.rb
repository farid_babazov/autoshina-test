# == Schema Information
#
# Table name: product_fields_values
#
#  id                  :integer          not null, primary key
#  product_id          :integer
#  categories_field_id :integer
#  value               :text
#  created_at          :datetime
#  updated_at          :datetime
#

class ProductFieldsValue < ActiveRecord::Base
  belongs_to :product
  belongs_to :categories_field
end
