# == Schema Information
#
# Table name: parts
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  categories_name :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class Part < ActiveRecord::Base
  has_many :products
  has_many :categories
  has_many :vendors, through: :categories 
end
