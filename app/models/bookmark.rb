# == Schema Information
#
# Table name: bookmarks
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#

class Bookmark < ActiveRecord::Base
  has_many :bookmark_items
  has_many :products, through: :bookmark_items
end
