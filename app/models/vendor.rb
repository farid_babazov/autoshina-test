# == Schema Information
#
# Table name: vendors
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category_id :integer
#  image       :string(255)
#  remote_id   :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Vendor < ActiveRecord::Base
  has_many :products
  belongs_to :category

  mount_uploader :image, VendorUploader
  validates_presence_of :name, :category_id

  default_scope -> { order(:name) }
end
