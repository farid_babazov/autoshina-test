# == Schema Information
#
# Table name: site_settings
#
#  id               :integer          not null, primary key
#  current_rate     :float
#  site_name        :string(255)
#  meta_description :string(255)
#  meta_keywords    :text
#  created_at       :datetime
#  updated_at       :datetime
#  phone            :string(255)
#  email            :string(255)
#  per_page         :integer
#

class SiteSetting < ActiveRecord::Base
end
