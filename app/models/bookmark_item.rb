# == Schema Information
#
# Table name: bookmark_items
#
#  id          :integer          not null, primary key
#  bookmark_id :integer
#  product_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class BookmarkItem < ActiveRecord::Base
  belongs_to :bookmark, touch: true
  belongs_to :product

  validates_presence_of :product_id, :bookmark_id
end
