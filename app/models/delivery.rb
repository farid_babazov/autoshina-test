# == Schema Information
#
# Table name: deliveries
#
#  id         :integer          not null, primary key
#  region     :string(255)
#  cost       :float
#  created_at :datetime
#  updated_at :datetime
#

class Delivery < ActiveRecord::Base
  has_many :orders, dependent: :nullify
end
