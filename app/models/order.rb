# == Schema Information
#
# Table name: orders
#
#  id              :integer          not null, primary key
#  status          :string(255)      default("unconfirmed")
#  name            :string(255)
#  address         :string(255)
#  phone           :string(255)
#  order_date      :datetime
#  created_at      :datetime
#  updated_at      :datetime
#  email           :string(255)
#  payment_type    :string(255)      default("cash")
#  delivery_cost   :float
#  delivery_id     :integer
#  delivery_region :string(255)
#

class Order < ActiveRecord::Base
  has_many :order_items
  has_many :products, through: :order_items
  has_one :payment
  belongs_to :delivery

  STATUSES = %i( unconfirmed confirmed delivered canceled )
  OUTDATED_SINCE = 1.days

  scope :outdated, -> { where("updated_at < ?", Time.now - OUTDATED_SINCE ) }

  validates :phone, format: { with: /[\+]\d{3}[\s]\d{3}[\s]\d{3}[\s]\d{3}/, message: 'invalid number'}, on: :update
  validates :address, length: { minimum: 6}, on: :update
  validates_presence_of :name, :address, :email, on: :update
  validates_presence_of :delivery_id, on: :update

  STATUSES.each_with_index do |status, index|
    scope status, -> { where status: status }
  end


  def total_price
    order_items.inject(0) do |sum, item|
      sum += item.product_cost.to_f * item.quantity.to_i
    end
  end

  def total_discounted_price_som
    order_items.inject(0) do |sum, item|
      sum += item.total_discounted_price.to_f
    end
  end

  def total_discounted_price_usd
    convert_to_usd(total_discounted_price_som)
  end

  def currency_total_price
    order_items.inject(0) do |sum, item|
      sum += item.product.conversion_of_currency.to_f * item.quantity.to_i
    end
  end

  def discount_total_price_s
    order_items.inject(0) do |sum, item|
      sum += item.product.discount_price.to_f * item.quantity.to_i
    end
  end

  def discount_total_price_d
    order_items.inject(0) do |sum, item|
      sum += item.product.currency_and_discount.to_f * item.quantity.to_i
    end
  end

  def color
    case self.status
      when 'unconfirmed'
        :gray
      when 'confirmed'
        :blue
      when 'delivered'
        :green
      when 'canceled'
        :red
    end
  end

  def self.order_statuses
    statuses = STATUSES.each_with_index.map{|status, index| [I18n.t("#{status}"), status] }
    statuses.shift
    statuses
  end

  # def self.cleanup!
  #   unconfirmed.outdated.destroy_all
  # end

  def self.cleanup!
    outdated.unconfirmed.find_each do |order|
      if order.payment_type == 'cash'
        order.order_items.destroy_all
        order.destroy
      end
    end
  end

  def delivery_price_som
    delivery_cost.nil? ? 0 : delivery_cost
  end

  def delivery_price_usd
    convert_to_usd(delivery_price_som)
  end

  def total_cost_som
    total_discounted_price_som + delivery_price_som
  end

  def total_cost_usd
    convert_to_usd(total_cost_som)
  end

  def convert_to_usd(sum)
    sum/SiteSetting.find(1).current_rate
  end
end
