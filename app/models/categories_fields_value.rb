# == Schema Information
#
# Table name: categories_fields_values
#
#  id                  :integer          not null, primary key
#  remote_id           :integer
#  categories_field_id :integer
#  value               :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#

class CategoriesFieldsValue < ActiveRecord::Base
  belongs_to :categories_field

  validates_presence_of :categories_field_id, :value
end
