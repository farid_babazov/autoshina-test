# == Schema Information
#
# Table name: prices
#
#  id         :integer          not null, primary key
#  file       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Price < ActiveRecord::Base
  mount_uploader :file, PriceUploader

  validates_presence_of :file 
end
