# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  internal_name    :string(255)
#  image            :string(255)
#  video            :string(255)
#  meta_keywords    :string(255)
#  meta_description :text
#  cost             :integer
#  count            :integer
#  vendor_id        :integer
#  category_id      :integer
#  last_edit        :datetime
#  remote_id        :integer
#  part_id          :integer
#  created_at       :datetime
#  updated_at       :datetime
#  featured         :integer          default(0)
#  discount         :integer          default(0)
#

class Product < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  belongs_to :category
  belongs_to :vendor
  belongs_to :part
  has_many :product_fields_values
  accepts_nested_attributes_for :product_fields_values, allow_destroy: true

  validates_presence_of :name, :internal_name, :part_id, :category_id, :vendor_id, :cost, :remote_id, :count
  validates :remote_id, uniqueness: true
  mount_uploader :image, ProductUploader

  paginates_per 24

  # default_scope -> { order("products.updated_at desc") }
  scope :featured, -> { where("featured > 0 and cost >  0").order("featured desc") }
  scope :price_exists, -> { where("products.cost > 0")}
  scope :tires, -> { where("part_id = 1") }
  scope :oils, -> { where("part_id = 2") }
  scope :accumulator, -> { where("part_id = 3") }
  scope :rims, -> { where("part_id = 4") }

  def get_value_by_field field
    self.product_fields_values.joins("LEFT JOIN categories_fields ON categories_fields.id = product_fields_values.categories_field_id").where("categories_fields.field_name = ?", field).first
  end

  def self.search(values=[], vendor="")
    values = (values || []).compact
    conditions = values.map{|val| "lower(product_fields_values.value) IN (" + val.map{|v| "'#{v.mb_chars.downcase.to_s}'" }.join(", ") + ")" }.map{|andc| "(" + andc + ")" }.join(" OR ")
    if values.present? and values.first.size > 0
      Product.joins(:product_fields_values).merge(ProductFieldsValue.where(conditions)).where(vendor).group('products.id').select("products.*").having('COUNT(*) = ?', values.first.size)
    else
      Product.where(vendor)
    end

  end

  def discount_price
    self.cost - ((self.cost/100) * self.discount)
  end

  def currency_price unit
    if unit == "dollar"
      number_to_currency self.conversion_of_currency, format: '%n%u', unit: "$"
    else
      number_to_currency self.cost, format: '%n %u', unit: "сом"
    end
  end

  def conversion_of_currency
    self.cost/SiteSetting.find(1).current_rate
  end

  def currency_and_discount
    c = self.conversion_of_currency.to_f
    c - ((c/100) * self.discount)
  end

  def is_tire?
    self.part_id == 1 ? true : false
  end
end
