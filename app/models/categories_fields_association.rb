# == Schema Information
#
# Table name: categories_fields_associations
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  category_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#  remote_id   :integer
#

class CategoriesFieldsAssociation < ActiveRecord::Base
  belongs_to :category
end
