class FeatureProduct

  def initialize(order)
    @order = order
  end

  def perform
    @order.products.each do |product|
      product.increment :featured, 50
      product.save
    end
  end
end
