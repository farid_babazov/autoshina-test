class UseThreedSecure

  def initialize
    @threed_secure = create_3d_secure
  end

  def create_3d_secure
    threed_secure = Ipc3dsec::ThreeDSecure.new({
      server_url: ENV['SERVER_URL'],
      merchant_url: ENV['MERCHANT_URL'],
      ssl_path: ENV['SSL_PATH'],
      ssl_keystore_name: AppConstants::SSL_KEYSTORE_NAME,
      ima_key_name: AppConstants::IMA_KEY_NAME,
      ssl_cert_name: AppConstants::SSL_CERT_NAME,
      ssl_pass: ENV['SSL_PASS']
    })
  end

  def get_transaction_ID(post_params)
    @threed_secure.get_transaction_ID(post_params)
  end

  def get_redirect_URL(transaction_id)
    @threed_secure.get_redirect_url(transaction_id)
  end

  def get_transaction_info(post_params)
    @threed_secure.get_transaction_info(post_params)
  end
end
