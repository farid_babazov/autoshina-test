# encoding: utf-8

class VendorUploader < CarrierWave::Uploader::Base
  include MiniMagick
  include CarrierWave::MiniMagick

  def store_dir
    "system/images/vendors/#{model.id}"
  end

  def default_url
    ActionController::Base.helpers.asset_path("fallback/image/" + [version_name, "no_foto.png"].compact.join('_'))
  end
  
  version :icon do
    process :resize_to_fill => [95, 25]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end
