# encoding: utf-8

class OfferUploader < CarrierWave::Uploader::Base

  include MiniMagick
  include CarrierWave::MiniMagick

  def store_dir
    "system/images/offers/#{model.id}"
  end

  def default_url
    ActionController::Base.helpers.asset_path("fallback/image/" + [version_name, "noimg.jpg"].compact.join('_'))
  end

  version :thumb do
    process :resize_to_fill => [145, 145]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end
