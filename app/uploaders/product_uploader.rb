# encoding: utf-8

class ProductUploader < CarrierWave::Uploader::Base
  include MiniMagick
  include CarrierWave::MiniMagick

  def store_dir
    "system/images/products/#{model.id}"
  end

  def default_url
    ActionController::Base.helpers.asset_path("fallback/image/" + [version_name, "noimg.jpg"].compact.join('_'))
  end
  
  version :thumb do
    process crop: '692x692+4+4'
    process :resize_to_fit => [125, 125]
  end

  version :show do
    process crop: '692x692+4+4'
    process :resize_to_fit => [700, 700]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  private

  def crop(geometry)
    manipulate! do |img|      
      img.crop(geometry)
      img
    end    
  end

  def resize_and_crop(size)  
    manipulate! do |image|                 
      if image[:width] < image[:height]
        remove = ((image[:height] - image[:width])/2).round 
        image.shave("0x#{remove}") 
      elsif image[:width] > image[:height] 
        remove = ((image[:width] - image[:height])/2).round
        image.shave("#{remove}x0")
      end
      image.resize("#{size}x#{size}")
      image
    end
  end
end
