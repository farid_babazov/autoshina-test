# encoding: utf-8

class CategoriesUploader < CarrierWave::Uploader::Base
  
  include MiniMagick
  include CarrierWave::MiniMagick

  def store_dir
    "system/images/categories/#{model.id}"
  end

  def default_url
    ActionController::Base.helpers.asset_path("fallback/image/" + [version_name, "no_foto.png"].compact.join('_'))
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end
