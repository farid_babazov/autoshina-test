# encoding: utf-8

class PriceUploader < CarrierWave::Uploader::Base

  require 'spreadsheet'
  Spreadsheet.client_encoding = 'UTF-8'

  def store_dir
    "system/prices/#{model.id}"
  end

  def extension_white_list
    %w(xls)
  end
  process :update_products

  def update_products
    # binding.pry
      book = Spreadsheet.open @file.path
      sheet1 = book.worksheet 0
      sheet1.each do |row|
        begin
          id = row[0].to_i
          cost = row[1].to_f
          count = row[2].to_i > 4 ? 4 : row[2].to_i
          p = Product.find_by(remote_id: id)
          unless p.nil?
            p.update_attributes cost: cost, count: count
            p.save
          end
        rescue NoMethodError => error
          Rails.logger.error error
        end
      end
  end
end
