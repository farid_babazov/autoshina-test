# encoding: utf-8

class FileUploader < CarrierWave::Uploader::Base

  include MiniMagick
  include CarrierWave::MiniMagick

  def store_dir
    "system/images/#{mounted_as}/#{model.id}"
  end
  
  version :slide, :if => :image? do
    process :resize_to_fill => [1280, 325]
  end

  version :catalog_placeholder, :if => :image? do
    process :resize_to_fill => [250, 250]
  end

  version :banner, :if => :image? do
    process :resize_to_fill => [840, 109]
  end

  version :icon, :if => :image? do
    process :resize_to_fill => [190, 34]
  end

  def extension_white_list
    %w(jpg jpeg gif png swf)
  end

  protected
    def image?(new_file)
      new_file.content_type.start_with? 'image'
    end
end
