# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190311094448) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body"
    t.string   "resource_id",   limit: 255, null: false
    t.string   "resource_type", limit: 255, null: false
    t.integer  "author_id"
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "alt_front_tires", force: :cascade do |t|
    t.integer  "id_auto"
    t.integer  "id_tire_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "alt_rear_tires", force: :cascade do |t|
    t.integer  "id_auto"
    t.integer  "id_tire_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "banners", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "link",       limit: 255
    t.string   "file",       limit: 255
    t.boolean  "active"
    t.string   "site",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order"
  end

  create_table "bookmark_items", force: :cascade do |t|
    t.integer  "bookmark_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bookmarks", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "car_manufacturers", force: :cascade do |t|
    t.integer  "remote_id"
    t.string   "title",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "car_models", force: :cascade do |t|
    t.integer  "car_manufacturer_id"
    t.string   "model",               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "car_modifications", force: :cascade do |t|
    t.integer  "car_model_id"
    t.string   "year",                limit: 255
    t.string   "modification",        limit: 255
    t.string   "default",             limit: 255
    t.string   "replacement",         limit: 255
    t.string   "tuning",              limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "car_manufacturer_id"
  end

  create_table "card_payment_errors", force: :cascade do |t|
    t.string   "code",        limit: 255
    t.string   "title",       limit: 255
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cars", force: :cascade do |t|
    t.integer  "remote_id"
    t.string   "vendor",       limit: 255
    t.string   "car",          limit: 255
    t.datetime "year"
    t.string   "modification", limit: 255
    t.string   "default",      limit: 255
    t.string   "replacement",  limit: 255
    t.string   "tuning",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "image",      limit: 255
    t.string   "order",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "remote_id"
    t.integer  "part_id"
  end

  create_table "categories_fields", force: :cascade do |t|
    t.integer  "remote_id"
    t.integer  "category_id"
    t.string   "field_name",     limit: 255
    t.integer  "association_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "show_in_browse"
    t.integer  "show_in_search"
    t.string   "type_field",     limit: 255
    t.string   "search_type",    limit: 255
  end

  create_table "categories_fields_associations", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "remote_id"
  end

  create_table "categories_fields_values", force: :cascade do |t|
    t.integer  "remote_id"
    t.integer  "categories_field_id"
    t.string   "value",               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "def_front_tires", force: :cascade do |t|
    t.integer  "id_auto"
    t.integer  "id_tire_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "def_rear_tires", force: :cascade do |t|
    t.integer  "id_auto"
    t.integer  "id_tire_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deliveries", force: :cascade do |t|
    t.string   "region",     limit: 255
    t.float    "cost"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_items", force: :cascade do |t|
    t.string   "title",            limit: 255
    t.string   "snippet",          limit: 255
    t.text     "text"
    t.string   "meta_keywords",    limit: 255
    t.string   "meta_description", limit: 255
    t.string   "image",            limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offer_items", force: :cascade do |t|
    t.string   "title"
    t.string   "snippet"
    t.text     "text"
    t.string   "meta_keywords"
    t.string   "meta_description"
    t.string   "image"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "order_items", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "product_id"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_name",          limit: 255
    t.string   "product_internal_name", limit: 255
    t.float    "product_cost"
    t.float    "product_discount",                  default: 0.0
  end

  create_table "orders", force: :cascade do |t|
    t.string   "status",          limit: 255, default: "unconfirmed"
    t.string   "name",            limit: 255
    t.string   "address",         limit: 255
    t.string   "phone",           limit: 255
    t.datetime "order_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",           limit: 255
    t.string   "payment_type",    limit: 255, default: "cash"
    t.float    "delivery_cost"
    t.integer  "delivery_id"
    t.string   "delivery_region", limit: 255
  end

  add_index "orders", ["delivery_id"], name: "index_orders_on_delivery_id", using: :btree

  create_table "parts", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "categories_name", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "transaction_id", limit: 255
    t.string   "result",         limit: 255
    t.string   "result_code",    limit: 255
    t.string   "rrn",            limit: 255
    t.string   "approval_code",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payments", ["order_id"], name: "index_payments_on_order_id", using: :btree

  create_table "prices", force: :cascade do |t|
    t.string   "file",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_fields_values", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "categories_field_id"
    t.text     "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.string   "internal_name",    limit: 255
    t.string   "image",            limit: 255
    t.string   "video",            limit: 255
    t.string   "meta_keywords",    limit: 255
    t.text     "meta_description"
    t.integer  "cost"
    t.integer  "count"
    t.integer  "vendor_id"
    t.integer  "category_id"
    t.datetime "last_edit"
    t.integer  "remote_id"
    t.integer  "part_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "featured",                     default: 0
    t.integer  "discount",                     default: 0
  end

  create_table "site_settings", force: :cascade do |t|
    t.float    "current_rate"
    t.string   "site_name",        limit: 255
    t.string   "meta_description", limit: 255
    t.text     "meta_keywords"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone",            limit: 255
    t.string   "email",            limit: 255
    t.integer  "per_page"
  end

  create_table "static_pages", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vendors", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.integer  "category_id"
    t.string   "image",       limit: 255
    t.integer  "remote_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
