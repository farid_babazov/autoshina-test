class AddCarManufacturerIdToCarModification < ActiveRecord::Migration
  def change
    add_column :car_modifications, :car_manufacturer_id, :integer
  end
end
