class CreateCategoriesFields < ActiveRecord::Migration
  def change
    create_table :categories_fields do |t|
      t.integer :remote_id
      t.integer :category_id
      t.string :field_name
      t.integer :association_id
      t.timestamps
    end
  end
end
