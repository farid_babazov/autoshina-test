class CreateSiteSettings < ActiveRecord::Migration
  def change
    create_table :site_settings do |t|
      t.float :current_rate
      t.string :site_name
      t.string :meta_description
      t.text :meta_keywords

      t.timestamps
    end
  end
end
