class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :title
      t.string :link
      t.string :file
      t.boolean :active
      t.string :site
      t.timestamps
    end
  end
end
