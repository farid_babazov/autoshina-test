class AddPerPageToSiteSettings < ActiveRecord::Migration
  def change
    add_column :site_settings, :per_page, :integer
  end
end
