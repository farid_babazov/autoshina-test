class CreateCarModels < ActiveRecord::Migration
  def change
    create_table :car_models do |t|
      t.integer :car_manufacturer_id
      t.string :model

      t.timestamps
    end
  end
end
