class AddRemoteIdToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :remote_id, :integer
  end
end
