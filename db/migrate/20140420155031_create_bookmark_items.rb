class CreateBookmarkItems < ActiveRecord::Migration
  def change
    create_table :bookmark_items do |t|
      t.integer :bookmark_id
      t.integer :product_id

      t.timestamps
    end
  end
end
