class CreateCarModifications < ActiveRecord::Migration
  def change
    create_table :car_modifications do |t|
      t.integer :car_model_id
      t.string :year
      t.string :modification
      t.string :default
      t.string :replacement
      t.string :tuning

      t.timestamps
    end
  end
end
