class AddRemoteIdToCategoriesFieldsAssociations < ActiveRecord::Migration
  def change
    add_column :categories_fields_associations, :remote_id, :integer
  end
end
