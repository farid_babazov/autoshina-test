class CreateCategoriesFieldsValues < ActiveRecord::Migration
  def change
    create_table :categories_fields_values do |t|
      t.integer :remote_id
      t.integer :categories_field_id
      t.string :value
      t.timestamps
    end
  end
end
