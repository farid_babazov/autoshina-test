class AddColumnsToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :product_name, :string
    add_column :order_items, :product_internal_name, :string
    add_column :order_items, :product_cost, :float
  end
end
