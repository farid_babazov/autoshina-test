class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :status
      t.string :name
      t.string :address
      t.string :phone
      t.datetime :order_date

      t.timestamps
    end
  end
end
