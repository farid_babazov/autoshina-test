class AddDeliveryRegionToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :delivery_region, :string
  end
end
