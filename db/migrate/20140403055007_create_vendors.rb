class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.string :name
      t.integer :category_id
      t.string :image
      t.integer :remote_id

      t.timestamps
    end
  end
end
