class RenameColumnFieldId < ActiveRecord::Migration
  def change
    rename_column :product_fields_values, :field_id, :categories_fields_id
  end
end
