class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :remote_id
      t.string :vendor
      t.string :car
      t.datetime :year
      t.string :modification
      t.string :default
      t.string :replacement
      t.string :tuning
      t.timestamps
    end
  end
end
