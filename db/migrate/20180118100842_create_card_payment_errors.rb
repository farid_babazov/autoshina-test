class CreateCardPaymentErrors < ActiveRecord::Migration
  def change
    create_table :card_payment_errors do |t|
      t.string :code
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
