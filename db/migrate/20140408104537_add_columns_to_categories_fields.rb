class AddColumnsToCategoriesFields < ActiveRecord::Migration
  def change
    add_column :categories_fields, :show_in_browse, :integer
    add_column :categories_fields, :show_in_search, :integer
    add_column :categories_fields, :type_field, :string
    add_column :categories_fields, :search_type, :string
  end
end
