class CreateCarManufacturers < ActiveRecord::Migration
  def change
    create_table :car_manufacturers do |t|
      t.integer :remote_id
      t.string :title, unique: true

      t.timestamps
    end
  end
end
