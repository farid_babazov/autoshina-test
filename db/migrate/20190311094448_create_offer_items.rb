class CreateOfferItems < ActiveRecord::Migration
  def change
    create_table :offer_items do |t|
      t.string :title
      t.string :snippet
      t.text :text
      t.string :meta_keywords
      t.string :meta_description
      t.string :image

      t.timestamps null: false
    end
  end
end
