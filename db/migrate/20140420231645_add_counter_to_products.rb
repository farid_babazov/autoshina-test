class AddCounterToProducts < ActiveRecord::Migration
  def change
    add_column :products, :featured, :integer, default: 0
    add_column :products, :discount, :integer, default: 0
  end
end
