class CreateProductFieldsValues < ActiveRecord::Migration
  def change
    create_table :product_fields_values do |t|
      t.integer :product_id
      t.integer :field_id
      t.text :value

      t.timestamps
    end
  end
end
