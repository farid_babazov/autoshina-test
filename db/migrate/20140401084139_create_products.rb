class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :internal_name
      t.string :image
      t.string :video
      t.string :meta_keywords
      t.text :meta_description
      t.integer :cost
      t.integer :count
      t.integer :vendor_id
      t.integer :category_id
      t.datetime :last_edit
      t.integer :remote_id
      t.integer :part_id
      t.timestamps
    end
  end
end
