class RenameColumnCategoriesFieldsId < ActiveRecord::Migration
  def change
    rename_column :product_fields_values, :categories_fields_id, :categories_field_id
  end
end
