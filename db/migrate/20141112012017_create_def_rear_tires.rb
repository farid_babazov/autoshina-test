class CreateDefRearTires < ActiveRecord::Migration
  def change
    create_table :def_rear_tires do |t|
      t.integer :id_auto
      t.integer :id_tire_size

      t.timestamps
    end
  end
end
