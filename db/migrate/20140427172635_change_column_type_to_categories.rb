class ChangeColumnTypeToCategories < ActiveRecord::Migration
  def change
    remove_column :categories, :part_id, :string
    add_column :categories, :part_id, :integer
  end
end
