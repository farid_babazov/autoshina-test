class AddProductDiscountToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :product_discount, :float, default: 0
  end
end
