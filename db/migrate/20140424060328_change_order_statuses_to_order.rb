class ChangeOrderStatusesToOrder < ActiveRecord::Migration
  def change
    change_column :orders, :status, :string, default: "unconfirmed"
  end
end
