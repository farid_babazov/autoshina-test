class AddColumnToSiteSettings < ActiveRecord::Migration
  def change
    add_column :site_settings, :phone, :string
    add_column :site_settings, :email, :string
  end
end
