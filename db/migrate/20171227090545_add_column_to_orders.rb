class AddColumnToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :email, :string
    add_column :orders, :payment_type, :string, default: 'cash'
    add_column :orders, :delivery_cost, :float
    add_reference :orders, :delivery, index: true
  end
end
