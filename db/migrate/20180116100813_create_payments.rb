class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :order, index: true
      t.string :transaction_id
      t.string :result
      t.string :result_code
      t.string :rrn
      t.string :approval_code

      t.timestamps
    end
  end
end
