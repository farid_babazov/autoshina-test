require 'csv'
namespace :db do
  desc 'Import payment errors from CSV'
  task card_payment_errors: :environment do
    csv_file = Rails.root.join( "db", "card_payment_errors.csv").to_s

    CSV.foreach(csv_file) do |row|
      CardPaymentError.create(code: row.first, title: row[1], description: row[2])
    end
  end
end
