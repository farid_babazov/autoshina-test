namespace :db do
  task populate: :environment do
    # start = DateTime.now
    # Part.create! name: "Шины", categories_name: "Тип автомобиля"
    # Part.create! name: "Автомасла", categories_name: "Тип масла"

    # #Categories
    # puts "Categories"
    # y = YAML.load  File.read ("#{Rails.root}/lib/shop_categories.yml")
    # y.each do |field|
    #     category = Category.new remote_id: field["id"], 
    #             name: field["name"],
    #             image: File.open(File.join(Rails.root, 'lib/tasks/images/categories', "#{field["image"]}")),
    #             part_id: field["part_id"],
    #             order: field["order"]
    #     category.save
    # end

    # #CategoriesFieldsAssociations
    # puts "CategoriesFieldsAssociations"
    # y = YAML.load  File.read ("#{Rails.root}/lib/shop_categories_fields_associations.yml")
    # y.each do |field|
    #     categories_fields_association = CategoriesFieldsAssociation.new category_id: Category.find_by_remote_id(field["category_id"]).id,
    #                                          name: field["name"],
    #                                          remote_id: field["id"]
    #     categories_fields_association.save
    # end

    # #CategoriesFields
    # puts "CategoriesFields"
    # y = YAML.load  File.read ("#{Rails.root}/lib/shop_categories_fields.yml")
    # y.each do |field|
    #   if field["association_id"] != 0
    #     categories_field = CategoriesField.new remote_id: field["id"], 
    #                                          category_id: Category.find_by_remote_id(field["category_id"]).id,
    #                                          field_name: field["field_name"],
    #                                          association_id: CategoriesFieldsAssociation.find_by_remote_id(field["association_id"]).id,
    #                                          show_in_browse: field["show_in_browse"],
    #                                          show_in_search: field["show_in_search"],
    #                                          type_field: field["type_field"],
    #                                          search_type: field["search_type"] 
    #   else
    #     categories_field = CategoriesField.new remote_id: field["id"], 
    #                                          category_id: Category.find_by_remote_id(field["category_id"]).id,
    #                                          field_name: field["field_name"],
    #                                          show_in_browse: field["show_in_browse"],
    #                                          show_in_search: field["show_in_search"],
    #                                          type_field: field["type_field"],
    #                                          search_type: field["search_type"] 
    #   end
    #     categories_field.save
    # end

    # #CategoriesFieldsValues
    # puts "CategoriesFieldsValues"
    # y = YAML.load  File.read ("#{Rails.root}/lib/shop_categories_fields_values.yml")
    # y.each do |field|
    #     cat_f_v = CategoriesField.find_by_remote_id(field["field_id"])
    #     if cat_f_v
    #         categories_fields_value = CategoriesFieldsValue.new remote_id: field["id"], 
    #                 categories_field_id: cat_f_v.id,
    #                 value: field["value"]

    #         categories_fields_value.save
    #     end
    # end


    # #Vendors
    # puts "Vendors"
    # y = YAML.load  File.read ("#{Rails.root}/lib/shop_vendors.yml")
    # y.each do |field|
    #     vendor = Vendor.new remote_id: field["id"], 
    #             name: field["name"],
    #             category_id: Category.find_by_remote_id(field["category_id"]).id,
    #             image: File.open(File.join(Rails.root, 'lib/tasks/images/vendors', "#{field["image"]}"))

    #    if vendor.save
    ##     puts vendor.name
    #   end
    # end

    # #Products
    # puts "Products"
    # y = YAML.load  File.read ("#{Rails.root}/lib/shop_items.yml")
    # y.each do |field|
    #   puts field
    #   product = Product.new remote_id: field["id"], 
    #             part_id: field["part_id"],
    #             category_id: Category.find_by_remote_id(field["category_id"]).id,
    #             name: field["name"],
    #             internal_name: field["internal_name"],
    #             count: field["count"],
    #             cost: field["cost"],
    #             image: File.open(File.join(Rails.root, 'lib/tasks/images/products', "#{field["image"]}")),
    #             video: field["video"],
    #             vendor_id: Vendor.find_by_remote_id(field["vendor_id"]).id,
    #             meta_keywords: field["meta_keywords"],
    #             meta_description: field["meta_description"]
    #   product.save
    # end

    # #Product_fields_values
    # puts "Product_fields_values"
    # y = YAML.load  File.read ("#{Rails.root}/lib/shop_items_fields_values.yml")
    # y.each do |field|
    #     product = Product.find_by_remote_id(field["item_id"])
    #     field_id = CategoriesField.find_by_remote_id(field["field_id"])
    #     if product && field_id
    #       product_fields_value = ProductFieldsValue.new product_id: product.id, categories_field_id: field_id.id, value: field["value"]
    #       product_fields_value.save
    #     end
    # end


    # # News
    # puts "News"
    # y = YAML.load  File.read ("#{Rails.root}/lib/news.yml")
    # y.each do |field|
    #   news = NewsItem.new title: field["title"],snippet: field["snippet"],text: field["news"],meta_keywords: field["meta_keywords"],meta_description: field["meta_description"],image: field["image"]
    #   news.save
    # end

    # # car_manufacturer
    # puts "car_manufacturer"
    # y = YAML.load  File.read ("#{Rails.root}/lib/cars_selection.yml")
    # y.each do |field|
    #     title = field["vendor"]
    #     manufacturer = CarManufacturer.new remote_id: field["id"],
    #                   title: title
    #                   # car: field["car"],
    #                   # year: field["year"],
    #                   # modification: field["modification"],
    #                   # default: field["default"],
    #                   # replacement: field["replacement"],
    #                   # tuning: field["tuning"]
    #     if manufacturer.save
    #         puts title
    #     end
    # end

    # # car_manufacturer
    # puts "car_model"
    # y = YAML.load  File.read ("#{Rails.root}/lib/cars_selection.yml")
    # y.each do |field|
    #     title = field["vendor"]
    #     if CarManufacturer.find_by title: title
    #         model = CarModel.new car_manufacturer_id: CarManufacturer.find_by(title: title).id,
    #                         model:  field["car"] 
    #     end
    #     if model.save
    #        puts field["car"]
    #     end
    # end

    # #car_modification
    # puts "car_modification"
    # y = YAML.load  File.read ("#{Rails.root}/lib/cars_selection.yml")
    # y.each do |field|
    #     model = field["car"]
    #     if CarModel.find_by model: model.to_s
    #         puts field
    #         car = CarModification.new car_model_id:  CarModel.find_by(model: model.to_s).id,
    #                             year: field["year"],
    #                             modification: field["modification"],
    #                             default: field["default"],
    #                             replacement: field["replacement"],
    #                             tuning: field["tuning"]
    #     end
    #     if car.save
    #     end
    # end
    # puts "---end---"
    # stop = DateTime.now
    # puts stop.to_i - start.to_i
    # 10.times do |p|
    #     Vendor.create name: "Создатель №#{p}", category_id: 19
    # end
    ctg1 = Category.create! name: "Аккумуляторы", part_id: 3
    CategoriesField.create! field_name: "Полярность", category_id: ctg1.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "Ёмкость", category_id: ctg1.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "Ток", category_id: ctg1.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "Размеры", category_id: ctg1.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "Клеммы", category_id: ctg1.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"

    ctg2 = Category.create! name: "Диски", part_id: 4
    CategoriesField.create! field_name: "Ширина", category_id: ctg2.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "Диаметр", category_id: ctg2.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "Крепеж(Разболтовка)", category_id: ctg2.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "Вылет(ET)", category_id: ctg2.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
    CategoriesField.create! field_name: "ЦО(Dia)", category_id: ctg2.id, show_in_browse: 1, show_in_search: 1, type_field: "select", search_type: "select"
  end
end