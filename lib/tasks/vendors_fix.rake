# bundle exec rake vendors_fix:create_general_category
namespace :vendors_fix do
  desc 'Create general tire category and add all tires to this category'
  task create_general_category: :environment do
    tire = Part.find_by(name: "Шины")
    categories = Category.where(part: tire).where('name != ?', 'Общие (Шины)')

    general_category = Category.find_or_create_by(name: 'Общие (Шины)', part: tire, order: 0)

    puts 'Start create CategoriesField'
    CategoriesField.where(category: categories).each do |categories_field|
      field_name = categories_field.field_name

      if field_name =~ /Ширина.*/i
        field_name = "Ширина"
      end
      if field_name =~ /Диаметр.*/i
        field_name = "Диаметр диска"
      end
      if field_name =~ /Высота.*/i
        field_name = "Высота"
      end

      cf = CategoriesField.find_by(category: general_category, field_name: field_name)
      if cf.nil?
        cf = CategoriesField.create(
          category: general_category,
          field_name: field_name,
          show_in_browse: categories_field.show_in_browse,
          type_field: categories_field.type_field,
          search_type: categories_field.search_type,
          show_in_search: categories_field.show_in_search
        )
      end
      categories_field.categories_fields_values.find_each do |categories_fields_value|
        cfv = CategoriesFieldsValue.find_by(categories_field: cf, value: categories_fields_value.value)
        if cfv.nil?
          CategoriesFieldsValue.create(
            categories_field: cf,
            value: categories_fields_value.value
          )
        end
      end

      categories_field.product_fields_values.find_each do |product_fields_value|
        pfv = ProductFieldsValue.find_by(product: product_fields_value.product, categories_field: cf, value: product_fields_value.value)
        if pfv.nil?
          ProductFieldsValue.create(
            product: product_fields_value.product,
            categories_field: cf,
            value: product_fields_value.value
          )
        end
        product_fields_value.destroy
      end

      puts '.'
    end
    puts 'End create CategoriesField'

    puts 'Start fix category & vendor in Product'
    vendors = Vendor.where(category: categories).select(:name).distinct.to_a
    vendors_lookup = {}
    vendors.each do |vendor|
      v = Vendor.find_or_create_by(name: vendor.name, category: general_category)
      vendors_lookup[v.name] = v
    end

    Product.includes(:vendor).tires.find_each do |product|
      vendor_name = product.vendor.name

      product.category = general_category
      product.vendor = vendors_lookup[vendor_name]

      product.save
      puts "Save product: #{product.id}"
    end
  end
end
