namespace :db do
  task new_tires: :environment do


    bc_tires_auto_brands_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_auto_brands_dict.yml")
    puts "bc_tires_auto_brands_dict"
    bc_tires_auto_models_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_auto_models_dict.yml")
    puts "bc_tires_auto_models_dict"
    # bc_tires_autos = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_autos (1).yml")
    puts "bc_tires_autos"
    bc_tires_auto_modifications_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_auto_modifications_dict.yml")
    puts "bc_tires_auto_modifications_dict"
    bc_tires_auto_years_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_auto_years_dict.yml")
    puts "bc_tires_auto_years_dict"
    bc_tires_tire_sizes_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_tire_sizes_dict.yml")
    puts "bc_tires_tire_sizes_dict"
    bc_tires_tire_widths_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_tire_widths_dict.yml")
    puts "bc_tires_tire_widths_dict"
    bc_tires_tire_heights_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_tire_heights_dict.yml")
    puts "bc_tires_tire_heights_dict"
    bc_tires_tire_diameters_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_tire_diameters_dict.yml")
    puts "bc_tires_tire_diameters_dict"
    #bc_tires_def_front_tires = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_def_front_tires.yml"); print "Read !"
    # bc_tires_alt_front_tires = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_alt_front_tires.yml"); print "Read "
    # bc_tires_def_rear_tires = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_def_rear_tires.yml"); print "Read "
    # bc_tires_alt_rear_tires = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_alt_rear_tires.yml"); print "Read "
    # bc_tires_wheel_rims_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_rims_dict.yml"); print "Read "
    # bc_tires_def_rear_wheels = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_def_rear_wheels.yml"); print "Read "
    # bc_tires_wheel_dias_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_dias_dict.yml"); print "Read "
    # bc_tires_auto_type_vehicles_dict = YAML.load  Fil2e.read ("#{Rails.root}/lib/tires/bc_tires_auto_type_vehicles_dict.yml"); print "Read "
    # bc_tires_wheel_fastener_types_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_fastener_types_dict.yml"); print "Read "
    # bc_tires_wheel_fastener_sizes_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_fastener_sizes_dict.yml"); print "Read "
    # bc_tires_wheel_pcd_number_of_studs_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_pcd_number_of_studs_dict.yml"); print "Read "
    # bc_tires_wheel_ets_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_ets_dict.yml"); print "Read "
    # bc_tires_wheel_diameters_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_diameters_dict.yml"); print "Read "
    # bc_tires_alt_front_wheels = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_alt_front_wheels.yml"); print "Read "
    # bc_tires_def_front_wheels = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_def_front_wheels.yml"); print "Read "
    # bc_tires_tire_speed_indexes_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_tire_speed_indexes_dict.yml"); print "Read "
    # bc_tires_wheel_pcds_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_pcds_dict.yml"); print "Read "
    # bc_tires_alt_rear_wheels = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_alt_rear_wheels.yml"); print "Read "
    # bc_tires_wheel_pcd_length_of_diameters_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_pcd_length_of_diameters_dict.yml"); print "Read "
    # bc_tires_tire_load_indexes_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_tire_load_indexes_dict.yml"); print "Read "
    # bc_tires_wheel_sizes_dict = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_wheel_sizes_dict.yml"); puts "Read!"

    # bc_tires_auto_brands_dict.each do |brand|
    #    car_brand = CarManufacturer.create! title: brand["value"]
    # end

    # bc_tires_auto_models_dict.each do |model|
    #    car_brand = CarModel.new model: model["value"], car_manufacturer_id: CarManufacturer
    # end


      file = "#{Rails.root}/public/new_tires"
    (4..13).each do |n|
      bc_tires_autos = YAML.load  File.read ("#{Rails.root}/lib/tires/bc_tires_autos (#{n}).yml")
      bc_tires_autos.each_with_index do |auto, index|

        begin
          # binding.pry
          width, height, diameter = 0
          def_sizes, alt_sizes = "", ""

          model = bc_tires_auto_models_dict.detect {|f| f["id"] == auto["id_model"] }
          brand = bc_tires_auto_brands_dict.detect {|f| f["id"] == auto["id_brand"] }
          modification = bc_tires_auto_modifications_dict.detect {|f| f["id"] == auto["id_modification"] }
          year = bc_tires_auto_years_dict.detect {|f| f["id"] == auto["id_year"] }

          def_front_tires = DefFrontTire.where(id_auto: auto["id"])
          def_rear_tires = DefRearTire.where(id_auto: auto["id"])
          alt_front_tires = AltFrontTire.where(id_auto: auto["id"])
          alt_rear_tires = AltRearTire.where(id_auto: auto["id"])

          def_front_tires.each do |t|
            tire_sizes = bc_tires_tire_sizes_dict.detect {|f| f["id"] == t.id_tire_size }
            width = bc_tires_tire_widths_dict.detect {|f| f["id"] == tire_sizes["id_width"] }
            height = bc_tires_tire_heights_dict.detect {|f| f["id"] == tire_sizes["id_height"] }
            diameter = bc_tires_tire_diameters_dict.detect {|f| f["id"] == tire_sizes["id_diameter"] }
            if width['value'].present? and height['value'].present? and diameter["value"].present?
              def_sizes  += "|" if def_sizes.size > 0
              def_sizes += "#{width['value']}/#{height['value']} R#{diameter["value"]}"
            end
          end

          def_rear_tires.each do |t|
            tire_sizes = bc_tires_tire_sizes_dict.detect {|f| f["id"] == t.id_tire_size }
            width = bc_tires_tire_widths_dict.detect {|f| f["id"] == tire_sizes["id_width"] }
            height = bc_tires_tire_heights_dict.detect {|f| f["id"] == tire_sizes["id_height"] }
            diameter = bc_tires_tire_diameters_dict.detect {|f| f["id"] == tire_sizes["id_diameter"] }
            if width['value'].present? and height['value'].present? and diameter["value"].present?
              def_sizes  += "|" if def_sizes.size > 0
              def_sizes += "#{width['value']}/#{height['value']} R#{diameter["value"]}"
            end
          end

          alt_front_tires.each do |t|
            tire_sizes = bc_tires_tire_sizes_dict.detect {|f| f["id"] == t.id_tire_size }
            width = bc_tires_tire_widths_dict.detect {|f| f["id"] == tire_sizes["id_width"] }
            height = bc_tires_tire_heights_dict.detect {|f| f["id"] == tire_sizes["id_height"] }
            diameter = bc_tires_tire_diameters_dict.detect {|f| f["id"] == tire_sizes["id_diameter"] }
            if width['value'].present? and height['value'].present? and diameter["value"].present?
              alt_sizes  += "|" if def_sizes.size > 0
              alt_sizes += "#{width['value']}/#{height['value']} R#{diameter["value"]}"
            end
          end
          alt_rear_tires.each do |t|
            tire_sizes = bc_tires_tire_sizes_dict.detect {|f| f["id"] == t.id_tire_size }
            width = bc_tires_tire_widths_dict.detect {|f| f["id"] == tire_sizes["id_width"] }
            height = bc_tires_tire_heights_dict.detect {|f| f["id"] == tire_sizes["id_height"] }
            diameter = bc_tires_tire_diameters_dict.detect {|f| f["id"] == tire_sizes["id_diameter"] }
            if width['value'].present? and height['value'].present? and diameter["value"].present?
              alt_sizes  += "|" if def_sizes.size > 0
              alt_sizes += "#{width['value']}/#{height['value']} R#{diameter["value"]}"
            end
          end

          car_brand = CarManufacturer.find_or_create_by title: "#{brand['value']}"
          car_model = CarModel.find_or_create_by model: "#{model['value']}", car_manufacturer: car_brand
          car_modification = CarModification.find_or_create_by  car_model: car_model, car_manufacturer: car_brand, year: "#{year["value"]}", modification: "#{modification["value"]}"

          if car_modification.default.present?
            default_sizes = "#{car_modification.default}|#{def_sizes}".split(/[|#]/).uniq.join("|")
          else
            default_sizes = def_sizes.split(/[|#]/).uniq.join("|")
          end

          if car_modification.replacement.present?
            replacement_sizes = "#{car_modification.replacement}|#{alt_sizes}""|".split(/[|#]/).uniq.join("|")
          else
            replacement_sizes = alt_sizes.split(/[|#]/).uniq.join("|")
          end

          car_modification.default = default_sizes
          car_modification.replacement = replacement_sizes
          car_modification.save
          puts "#{n}|#{index} #{brand['value']} | #{model['value']} | #{modification["value"]} | #{year["value"]}"
          puts default_sizes
          puts replacement_sizes
          puts "-"*100
        rescue => e
          File.open(file, 'w:UTF-8') do |file| 
            file.write(e)
            file.write("\n" + auto.to_s + "\n")
            file.write("-"*100 + "\n")
          end
        end
      end
    end
  end
end

#CarModel.count = 789
#CarManufacturer.count = 64
#CarModification.count = 17768
#111043