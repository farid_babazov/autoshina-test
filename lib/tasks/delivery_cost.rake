require 'csv'
namespace :db do
  desc 'Import delivery costs from CSV'
  task delivery_cost: :environment do
    csv_file = Rails.root.join( "db", "delivery_costs.csv").to_s

    CSV.foreach(csv_file) do |row|
      Delivery.create(region: row.first, cost: row[1])
    end
  end
end
