module Ipc3dsec

  class ThreeDSecure
    require 'rack'
    require 'action_dispatch'
    require 'erb'
    require 'openssl'
    require 'faraday'

    REQUIRED_PARAMS = [
      'ssl_path', 'ssl_pass', 'ssl_cert_name', 'merchant_url',
      'server_url', 'ssl_keystore_name', 'ima_key_name'
    ]

    def initialize(params = {})
      raise ArgumentError, "Not found requirements params" unless validate!(params)

      @merchant_url = params[:merchant_url]
      @server_url = params[:server_url]
      @ssl_path = params[:ssl_path]
      @ssl_pass = params[:ssl_pass]
      @ssl_cert_name = params[:ssl_cert_name]
      @ssl_keystore_name = params[:ssl_keystore_name]
      @ima_key_name = params[:ima_key_name]
    end

    def get_transaction_ID(user_to_post)
      to_post = get_transaction_query(user_to_post)

      if to_post.size > 0
        response = get_IMA(to_post)
        if response.present?
          response = render_XML_response(response.body)
          if response[:transaction_id].present?
            transaсtion_id = response[:transaction_id]
            return transaсtion_id
          else
            return false
          end
        else
          begin
            raise Exception.new("No response")
          rescue Exception => error
            Rails.logger.error error
            return 'No response'
          end
        end
      else
        return false
      end
    end

    def get_transaction_info(user_to_post)
      response = ''
      user_to_post.merge!({
        command: 'C',
        msg_type: nil
        })
      to_post = get_transaction_query(user_to_post)
      if to_post.size > 0
        response = get_IMA(to_post)

        response = render_XML_response(response.body) unless response == ''
      end
      response
    end

    def get_redirect_url(transation_id)
      @server_url + "?trans_id=" + ERB::Util.url_encode(transation_id)
    end

  private

    def validate!(params)
      validated = true
      REQUIRED_PARAMS.each do |param|
        begin
          raise ArgumentError, "#{param} is required, but it is missing" unless params[:"#{param}"].present?
        rescue ArgumentError => e
          p e.message
          validated = false
        end
      end
      return validated
    end

    def get_transaction_query(user_to_post)
      to_post = {}

      client_IP = user_to_post[:client_ip_addr]
      if client_IP
        to_post = {
          command: 'v',
          msg_type: "SMS"
        }
        to_post.merge!(user_to_post)
      end
      to_post
    end

    def get_IMA(fields)
      response = ''

      url = @merchant_url

      # location of keys + keyphrase
      ssl_keystore = @ssl_path + @ssl_keystore_name
      ssl_cert = @ssl_path + @ssl_cert_name
      ima_key = @ssl_path + @ima_key_name

      if get_private_key(ssl_keystore, ima_key)

        ssl_options =  {
          verify: false,
          client_key: OpenSSL::PKey::RSA.new(File.read(ima_key)),
          client_cert: OpenSSL::X509::Certificate.new(File.read(ssl_cert)),
          private_key: @ssl_pass
        }

        connection = Faraday.new(url: url, ssl: ssl_options, params: fields)
        begin
          response = connection.post
        rescue Faraday::ClientError => error
          Rails.logger.error error
          response = ''
        end
      end

      remove_private_key(ima_key)
      response
    end

    def get_private_key(ssl_keystore, ima_key)
      cert_info = get_certificate_info(ssl_keystore)

        if (ima_key && cert_info && cert_info.key)
          output_pkey = File.new(ima_key, 'w')
          output_pkey.write(cert_info.key.export)
          output_pkey.close
          return true
        end
      return false
    end

    def remove_private_key(ima_key)
      if (ima_key && File.exists?(ima_key))
        File.delete(ima_key)
        return true
      end
      return false
    end

    def get_certificate_info(ssl_keystore)
      cert_info = ''

      if File.exist?(ssl_keystore)
        keystore_data = File.read(ssl_keystore)
        cert_info = OpenSSL::PKCS12.new(keystore_data, @ssl_pass)
      end

      cert_info
    end

    # get MAIN values from response
    def render_XML_response(response)
      matches = response.match(/([^\]]*)/i)
      # matches = response.body.match(/([^\]]*)/i)

      result = {}

      result[:transaction_id] = get_XML_value('TRANSACTION_ID: ([\S]*)', matches[1], '')
      result[:result] = get_XML_value('RESULT: ([\S]*)', matches[1], '')
      result[:result_code] = get_XML_value('RESULT_CODE: ([\S]*)', matches[1], '')
      result[:rrn] = get_XML_value('RRN: ([\S]*)', matches[1], '')
      result[:approval_code] = get_XML_value('APPROVAL_CODE: ([\S]*)', matches[1], '')
      result[:RECC_PMNT_ID] = get_XML_value('RECC_PMNT_ID: ([\S]*)', matches[1], '')
      result[:RECC_PMNT_EXPIRY] = get_XML_value('RECC_PMNT_EXPIRY: ([\S]*)', matches[1], '')
      result[:FLD_075] = get_XML_value('FLD_075: ([\S]*)', matches[1], '')
      result[:FLD_076] = get_XML_value('FLD_076: ([\S]*)', matches[1], '')
      result[:FLD_087] = get_XML_value('FLD_087: ([\S]*)', matches[1], '')
      result[:FLD_088] = get_XML_value('FLD_088: ([\S]*)', matches[1], '')
      result[:FLD_039] = get_XML_value('FLD_039: ([\S]*)', matches[1], '')

      result
    end

    def get_XML_value(regex, params, default)
      matches = params.scan(/#{regex}/i)

      if matches.size > 0
        return matches[0][0]
      else
        return default
      end
    end
  end
end
